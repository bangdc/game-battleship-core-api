package docongbang.com.battleship.api.ship;

import java.util.ArrayList;

import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.player.Player;
import docongbang.com.battleship.api.utils.ShipOrientation;

/**
 * The interface of Ship.
 * 
 * @author bangdc
 * @since 29-12-2016
 */
public interface Ship {
	/**
	 * @return the length of the ship
	 */
	public int getLength();

	/**
	 * @return the width of the ship
	 */
	public int getWidth();

	/**
	 * Set the length of ship
	 * 
	 * @param length
	 */
	public void setLength(final int length);

	/**
	 * Set the width of ship
	 * 
	 * @param width
	 */
	public void setWidth(final int width);

	/**
	 * @return the name of the ship
	 */
	public String getName();

	/**
	 * @return Check whether the ship is still alive or not.
	 */
	public boolean isAlive();

	/**
	 * The current health of this ship is the total health of every cells where
	 * it is located on.
	 * 
	 * @return the current health of the ship
	 */
	public int getHealth();

	/**
	 * @return the player / captain who owns ship
	 */
	public Player getCaptain();

	/**
	 * Set the captain / player of this ship Each ship must belong to only one
	 * player.
	 * 
	 * @param captain
	 *            Reference to a player.
	 */
	public void setCaptain(Player captain);

	/**
	 * Reset the position of this ship
	 */
	public void resetPosition();

	/**
	 * Add a cell to the cells of this ship Require: - cell must reference to
	 * ship
	 */
	public void addCell(final CellTrackingFleet cell);

	/**
	 * @return All cells of that the ship is located on.
	 */
	public ArrayList<CellTrackingFleet> getCells();

	/**
	 * Check whether the ship was placed on a grid.
	 * 
	 * @return true or false.
	 */
	public boolean wasPlacedOnGrid();

	/**
	 * Upgrade a ship at a specific cell
	 * 
	 * @param cell
	 */
	public void upgrade(CellTrackingFleet cell);

	/**
	 * Get origin of the ship
	 * 
	 * @return
	 */
	public CellTrackingFleet getOrigin();

	/**
	 * Set the origin of the ship
	 * 
	 * @param origin
	 */
	public void setOrigin(CellTrackingFleet origin);

	/**
	 * Get the orientation of the ship
	 * 
	 * @return
	 */
	public ShipOrientation getOrientation();

	/**
	 * Set the orientation of the ship
	 * 
	 * @param orientation
	 */
	public void setOrientation(ShipOrientation orientation);
}
