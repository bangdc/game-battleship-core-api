package docongbang.com.battleship.api.ship.naval.exceptions;

public class OnlySubmarineHaveIlluminatingRocket extends Exception {
	private static final long serialVersionUID = 2494010927949313794L;

	public OnlySubmarineHaveIlluminatingRocket(String message) {
		super(message);
	}
}
