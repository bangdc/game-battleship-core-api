package docongbang.com.battleship.api.ship.naval;

import docongbang.com.battleship.api.player.Player;

public class Battleship extends BasicNavalShip {
	private final static int WIDTH = 1;
	private final static int LENGTH = 4;

	public Battleship(Player captain) {
		super("Battleship", WIDTH, LENGTH, captain);
	}
}
