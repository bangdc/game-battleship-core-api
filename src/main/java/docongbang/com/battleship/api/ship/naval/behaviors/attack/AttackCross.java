package docongbang.com.battleship.api.ship.naval.behaviors.attack;

import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import docongbang.com.battleship.api.game.Game;
import docongbang.com.battleship.api.grid.GridTrackingFleet;
import docongbang.com.battleship.api.grid.cell.CellBomb;
import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.player.Player;
import docongbang.com.battleship.api.ship.naval.NavalShip;
import docongbang.com.battleship.api.utils.Helper;

/**
 * A concrete attack strategy of ship. This attack strategy will cause damages
 * for a target cell, as well as the 4 others (Top, Bottom, Left, Right)
 * 
 * @author bangdc
 * @since 29-12-2016
 */
public class AttackCross implements AttackStrategy {
	private static final Logger LOG = LogManager.getLogger(AttackCross.class);
	private NavalShip dutyShip;

	/**
	 * Construct the strategy
	 * 
	 * @param dutyShip
	 *            The ship carrying out the duty
	 */
	public AttackCross(NavalShip dutyShip) {
		this.dutyShip = dutyShip;
	}

	/**
	 * The actual targets are:
	 * <ul>
	 * <li>The target cell - as the center</li>
	 * <li>The upper cell</li>
	 * <li>The lower cell</li>
	 * <li>The right cell</li>
	 * <li>The left cell</li>
	 * </ul>
	 * Treating individually each target as in attack classic .
	 */
	@Override
	public void doAttack(CellTrackingFleet target, int damage) {
		final Player captain = dutyShip.getCaptain();
		final Player opponent = captain.getOpponent();
		final GridTrackingFleet gridTrackingFleetOfOpponent = opponent.getGridTrackingFleet();

		boolean causedCivilShipDestroyed = opponent.getSailingShip().getHealth() > 0;

		if (Game.DEBUGGABLE) {
			LOG.info(captain.getName() + " is attacking " + opponent.getName() + " by CROSS strategy with ship "
					+ dutyShip.getName() + " at position " + target.toString());
		}

		final ArrayList<CellTrackingFleet> targets = new ArrayList<CellTrackingFleet>();

		final int centerColIndex = target.getColIndex();
		final int centerRowIndex = target.getRowIndex();

		targets.add(target);

		if (centerRowIndex - 1 >= 0) {
			final int topCellIndex = Helper.calculatePairingNumber(centerColIndex, centerRowIndex - 1);
			targets.add(gridTrackingFleetOfOpponent.getCells().get(topCellIndex));
		}
		if (centerRowIndex + 1 < Game.GRID_HEIGHT) {
			final int bottomCellIndex = Helper.calculatePairingNumber(centerColIndex, centerRowIndex + 1);
			targets.add(gridTrackingFleetOfOpponent.getCells().get(bottomCellIndex));
		}
		if (centerColIndex - 1 >= 0) {
			final int leftCellIndex = Helper.calculatePairingNumber(centerColIndex - 1, centerRowIndex);
			targets.add(gridTrackingFleetOfOpponent.getCells().get(leftCellIndex));
		}
		if (centerColIndex + 1 < Game.GRID_WIDTH) {
			final int rightCellIndex = Helper.calculatePairingNumber(centerColIndex + 1, centerRowIndex);
			targets.add(gridTrackingFleetOfOpponent.getCells().get(rightCellIndex));
		}

		for (CellTrackingFleet actualTarget : targets) {
			actualTarget.setNumberOfTimesBeingAttacked(actualTarget.getNumberOfTimesBeingAttacked() + 1);
			if (actualTarget instanceof CellBomb) {
				CellBomb targetWithBomb = (CellBomb) actualTarget;

				if (Game.DEBUGGABLE) {
					LOG.info(captain.getName() + " TRIGGERED THE BOMB of " + opponent.getName());
				}
				if (targetWithBomb.wasTriggered()) {
					if (Game.DEBUGGABLE) {
						LOG.info("The bomb was already triggered, then treat being attacked as normal cell.");
					}
				} else {
					Hashtable<Integer, ArrayList<CellTrackingFleet>> surroundingCells = opponent.getGridTrackingFleet()
							.getSurroundingCells(actualTarget, Game.BOMB_IMPACT_LEVEL);
					for (Integer surroundingLevel : surroundingCells.keySet()) {
						for (CellTrackingFleet cell : surroundingCells.get(new Integer(surroundingLevel))) {
							cell.setNumberOfTimesBeingAttacked(cell.getNumberOfTimesBeingAttacked() + 1);
							cell.setHealth(cell.getHealth() - (Game.BOMB_DAMAGE - surroundingLevel.intValue()));
						}
					}
					actualTarget.setHealth(actualTarget.getHealth() - Game.BOMB_DAMAGE);
					targetWithBomb.setTriggered();
				}
			} else {
				actualTarget.setHealth(actualTarget.getHealth() - damage);
			}
		}
		causedCivilShipDestroyed = causedCivilShipDestroyed && opponent.getSailingShip().getHealth() == 0;
		if (causedCivilShipDestroyed) {
			if (Game.DEBUGGABLE) {
				LOG.info(captain.getName() + " caused the DESTROY OF SAILING SHIP on grid of " + opponent.getName());
			}
			captain.setNumberOfTurnsRemaining(captain.getNumberOfTurnsRemaining() - Game.COST_DESTROYING_CIVIL_SHIP);
			opponent.setNumberOfTurnsRemaining(opponent.getNumberOfTurnsRemaining() + Game.COST_DESTROYING_CIVIL_SHIP);
			if (Game.DEBUGGABLE) {
				LOG.info(captain.getName() + " then will have number of turns remaining: "
						+ captain.getNumberOfTurnsRemaining());
			}
		}

		captain.setNumberOfTurnsRemaining(captain.getNumberOfTurnsRemaining() - Game.COST_ATTACKING_CROSS);
		opponent.setNumberOfTurnsRemaining(opponent.getNumberOfTurnsRemaining() + Game.COST_ATTACKING_CROSS);
	}
}
