package docongbang.com.battleship.api.ship.naval;

import docongbang.com.battleship.api.player.Player;

public class Carrier extends BasicNavalShip {
	private final static int WIDTH = 1;
	private final static int LENGTH = 5;

	public Carrier(Player captain) {
		super("Carrier", WIDTH, LENGTH, captain);
	}
}
