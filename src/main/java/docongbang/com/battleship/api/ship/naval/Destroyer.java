package docongbang.com.battleship.api.ship.naval;

import docongbang.com.battleship.api.player.Player;

public class Destroyer extends BasicNavalShip {
	private final static int WIDTH = 1;
	private final static int LENGTH = 2;

	public Destroyer(Player captain) {
		super("Destroyer", WIDTH, LENGTH, captain);
	}
}
