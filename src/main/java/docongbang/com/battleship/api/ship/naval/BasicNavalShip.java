package docongbang.com.battleship.api.ship.naval;

import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.player.Player;
import docongbang.com.battleship.api.ship.BasicShip;
import docongbang.com.battleship.api.ship.naval.behaviors.attack.AttackStrategy;
import docongbang.com.battleship.api.ship.naval.exceptions.RunOutOfRocket;

public class BasicNavalShip extends BasicShip implements NavalShip {
	private AttackStrategy attackStrategy;

	public BasicNavalShip(String name, int width, int length, Player captain) {
		super(name, width, length, captain);
	}

	@Override
	public void setAttackStrategy(AttackStrategy attackStrategy) {
		this.attackStrategy = attackStrategy;
	}

	@Override
	public AttackStrategy getAttackStrategy() {
		return attackStrategy;
	}

	@Override
	public void attack(CellTrackingFleet target, int damage) {
		try {
			attackStrategy.doAttack(target, damage);
		} catch (RunOutOfRocket e) {
			e.printStackTrace();
		}
	}

	@Override
	public int getCapabilityOfKilling() {
		return getHealth();
	}
}
