package docongbang.com.battleship.api.ship.naval;

import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.ship.Ship;
import docongbang.com.battleship.api.ship.naval.behaviors.attack.AttackStrategy;

/**
 * The interface of naval ship
 * 
 * @author bangdc
 *
 */
public interface NavalShip extends Ship {
	/**
	 * An attack carried out by this naval ship.
	 * 
	 * @param target
	 *            The target position
	 * @param damage
	 *            The damage caused to that target
	 */
	public void attack(CellTrackingFleet target, int damage);

	/**
	 * Get the capability of killing of naval ship for each attack.
	 * 
	 * @return
	 */
	public int getCapabilityOfKilling();

	/**
	 * Get the attack strategy of ship
	 * 
	 * @return
	 */
	public AttackStrategy getAttackStrategy();

	/**
	 * Set the attack strategy of ship
	 * 
	 * @param attackStrategy
	 */
	public void setAttackStrategy(AttackStrategy attackStrategy);
}
