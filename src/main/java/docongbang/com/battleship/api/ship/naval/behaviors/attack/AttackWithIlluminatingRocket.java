package docongbang.com.battleship.api.ship.naval.behaviors.attack;

import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import docongbang.com.battleship.api.game.Game;
import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.player.Player;
import docongbang.com.battleship.api.ship.naval.SubmarineWithIlluminatingRocket;
import docongbang.com.battleship.api.ship.naval.exceptions.RunOutOfRocket;

public class AttackWithIlluminatingRocket implements AttackStrategy {
	private static final Logger LOG = LogManager.getLogger(AttackWithIlluminatingRocket.class);
	private SubmarineWithIlluminatingRocket dutyShip;

	/**
	 * Construct the strategy
	 * 
	 * @param dutyShip
	 *            The ship carrying out the duty
	 */
	public AttackWithIlluminatingRocket(SubmarineWithIlluminatingRocket dutyShip) {
		this.dutyShip = dutyShip;
	}

	/**
	 * <p>
	 * Illuminate the range of cells around the target. The wide of range
	 * depends on the damage. For example if damage is 1, then the maximum 9
	 * cells including the center target will be visible. For example if damage
	 * is 2, then the maximum 25 cells including the center target will be
	 * visible. So on.
	 * </p>
	 * <b>This attacking strategy will cause no damage for the involved
	 * cells.</b>
	 */
	@Override
	public void doAttack(CellTrackingFleet target, int damage) throws RunOutOfRocket {
		final Player captain = dutyShip.getCaptain();
		final Player opponent = captain.getOpponent();
		LOG.info("Attack Rocket");
		if (dutyShip.getNumberOfRockets() > 0) {
			if (Game.DEBUGGABLE) {
				LOG.info(
						captain.getName() + " is attacking " + opponent.getName() + " with ILLUMINATING ROCKET of ship "
								+ dutyShip.getName() + " at position " + target.toString());
			}

			// Illuminate the cells
			Hashtable<Integer, ArrayList<CellTrackingFleet>> surroundingCells = opponent.getGridTrackingFleet()
					.getSurroundingCells(target, damage);

			for (Integer surroundingLevel : surroundingCells.keySet()) {
				for (CellTrackingFleet cell : surroundingCells.get(surroundingLevel)) {
					cell.setVisible();
				}
			}

			// Set visible the target cell.
			target.setVisible();
			// After using the rocket, it will be removed from the submarine
			// ship.
			dutyShip.setNumberOfRockets(dutyShip.getNumberOfRockets() - 1);

			// No matter what happens, after using this strategy, current player
			// must lost NUMBER_OF_TURNS_COST, and opponent player must gain
			// NUMBER_OF_TURNS_COST.
			captain.setNumberOfTurnsRemaining(captain.getNumberOfTurnsRemaining() - Game.COST_USING_ROCKET);
			opponent.setNumberOfTurnsRemaining(opponent.getNumberOfTurnsRemaining() + Game.COST_USING_ROCKET);
		} else {
			captain.setNumberOfTurnsRemaining(captain.getNumberOfTurnsRemaining() - 1);
			opponent.setNumberOfTurnsRemaining(opponent.getNumberOfTurnsRemaining() + 1);
			throw new RunOutOfRocket(dutyShip.getName() + " does not have any rocket");
		}
	}
}
