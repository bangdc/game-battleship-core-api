package docongbang.com.battleship.api.ship.naval.exceptions;

public class RunOutOfRocket extends Exception {
	private static final long serialVersionUID = 908195892067080730L;
	public RunOutOfRocket(String message) {
		super(message);
	}
}
