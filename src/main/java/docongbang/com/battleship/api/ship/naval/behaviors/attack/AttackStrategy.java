package docongbang.com.battleship.api.ship.naval.behaviors.attack;

import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.ship.naval.exceptions.RunOutOfRocket;

/**
 * Interface of attack strategy.
 * 
 * @author bangdc
 * @since 29-12-2016
 *
 */
public interface AttackStrategy {
	/**
	 * Attack a target and cause damage for the ship if it is in the range.
	 * 
	 * @param target
	 * @param damage
	 * @throws RunOutOfRocket
	 */
	public void doAttack(CellTrackingFleet target, int damage) throws RunOutOfRocket;
}
