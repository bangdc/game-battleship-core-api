package docongbang.com.battleship.api.ship;

import docongbang.com.battleship.api.player.Player;
import docongbang.com.battleship.api.ship.naval.Battleship;
import docongbang.com.battleship.api.ship.naval.Carrier;
import docongbang.com.battleship.api.ship.naval.Cruiser;
import docongbang.com.battleship.api.ship.naval.Destroyer;
import docongbang.com.battleship.api.ship.naval.NavalShip;
import docongbang.com.battleship.api.ship.naval.Submarine;
import docongbang.com.battleship.api.ship.naval.SubmarineWithIlluminatingRocket;
import docongbang.com.battleship.api.ship.naval.exceptions.FactoryCouldNotProduceTheShip;
import docongbang.com.battleship.api.ship.naval.exceptions.OnlySubmarineHaveIlluminatingRocket;

/**
 * The factory is charged for producing ship based on the shipType and captain.
 * 
 * @author bangdc
 *
 */
public class ShipFactory {
	/**
	 * Producing a naval ship
	 * 
	 * @param shipType
	 *            The type of ship can be:
	 *            <ul>
	 *            <li>carrier</li>
	 *            <li>cruiser</li>
	 *            <li>destroyer</li>
	 *            <li>submarine</li>
	 *            <li>submarineWithIlluminatingRocket</li>
	 *            <li>battleship</li>
	 *            </ul>
	 * @param captain
	 * @return
	 * @throws OnlySubmarineHaveIlluminatingRocket
	 * @throws FactoryCouldNotProduceTheShip
	 */
	public static NavalShip createNavalShip(final String shipType, final Player captain)
			throws OnlySubmarineHaveIlluminatingRocket, FactoryCouldNotProduceTheShip {
		if (shipType.equals("carrier")) {
			return new Carrier(captain);
		}
		if (shipType.equals("cruiser")) {
			return new Cruiser(captain);
		}
		if (shipType.equals("destroyer")) {
			return new Destroyer(captain);
		}
		if (shipType.equals("submarine")) {
			return new Submarine(captain);
		}
		if (shipType.equals("submarineWithIlluminatingRocket")) {
			NavalShip ship = new Submarine(captain);
			return new SubmarineWithIlluminatingRocket(ship);
		}
		if (shipType.equals("battleship")) {
			return new Battleship(captain);
		}
		throw new FactoryCouldNotProduceTheShip("Factory does not know how to produce " + shipType);
		// switch (shipType) {
		// case "carrier":
		// return new Carrier(captain);
		// case "cruiser":
		// return new Cruiser(captain);
		// case "destroyer":
		// return new Destroyer(captain);
		// case "submarine":
		// return new Submarine(captain);
		// case "submarineWithIlluminatingRocket":
		// NavalShip ship = new Submarine(captain);
		// return new SubmarineWithIlluminatingRocket(ship);
		// case "battleship":
		// return new Battleship(captain);
		// default:
		// throw new FactoryCouldNotProduceTheShip("Factory does not know how to
		// produce " + shipType);
		// }
	}
}
