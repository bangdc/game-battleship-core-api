package docongbang.com.battleship.api.ship.civil;

import docongbang.com.battleship.api.ship.Ship;

/**
 * The interface of civil ship
 * 
 * @author bangdc
 * @see Ship
 */
public interface CivilShip extends Ship {
}
