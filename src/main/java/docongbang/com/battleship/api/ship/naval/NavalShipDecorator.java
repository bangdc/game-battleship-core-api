package docongbang.com.battleship.api.ship.naval;

import docongbang.com.battleship.api.ship.naval.behaviors.attack.AttackStrategy;

public abstract class NavalShipDecorator implements NavalShip {
	private NavalShip ship;

	public NavalShipDecorator(final NavalShip ship) {
		this.ship = ship;
	}

	@Override
	public AttackStrategy getAttackStrategy() {
		return ship.getAttackStrategy();
	}

	@Override
	public int getCapabilityOfKilling() {
		return ship.getCapabilityOfKilling();
	}

	@Override
	public void setAttackStrategy(AttackStrategy attackStrategy) {
		ship.setAttackStrategy(attackStrategy);
	}

	/**
	 * Get the decorated ship.
	 * 
	 * @return
	 */
	public NavalShip getDecoratedShip() {
		return ship;
	}
}
