package docongbang.com.battleship.api.ship.civil;

import docongbang.com.battleship.api.player.Player;
import docongbang.com.battleship.api.ship.BasicShip;

/**
 * The sailing ship is a kind of civil ship. I apply the Singleton pattern here,
 * so that each player will have ONLY one sailing ship on his/her grid.
 * 
 * @author bangdc
 *
 */
public class SailingShip extends BasicShip implements CivilShip {
	private final static int SAILING_SHIP_WIDTH = 1;
	private final static int SAILING_SHIP_LENGTH = 1;
	private final static int SAILING_SHIP_HEALTH = 4;
	private static SailingShip sailingShipOfPlayer1;
	private static SailingShip sailingShipOfPlayer2;

	private SailingShip(String name, Player captain) {
		super(name, SAILING_SHIP_WIDTH, SAILING_SHIP_LENGTH, captain);
	}

	/**
	 * Get the sailing ship that will be placed on the grid of player 1.
	 * 
	 * @return
	 */
	public static SailingShip getSailingShipOfPlayer1(Player player1) {
		if (sailingShipOfPlayer1 == null) {
			sailingShipOfPlayer1 = new SailingShip("The Sailing Ship 1", player1);
		}
		return sailingShipOfPlayer1;
	}

	/**
	 * Get the sailing ship that will be placed on the grid of player 2
	 * 
	 * @return
	 */
	public static SailingShip getSailingShipOnPlayer2(Player player2) {
		if (sailingShipOfPlayer2 == null) {
			sailingShipOfPlayer2 = new SailingShip("The Sailing Ship 2", player2);
		}
		return sailingShipOfPlayer2;
	}

	/**
	 * Initialize the health for this sailing ship.
	 */
	public void initHealth() {
		// because the ship acquires only one CELL.
		// getCells().get(0).increaseHealth(SAILING_SHIP_HEALTH);
		getCells().get(0).setHealth(SAILING_SHIP_HEALTH);
	}

	/**
	 * Reset this class for creating new game.
	 */
	public static void reset() {
		sailingShipOfPlayer1 = null;
		sailingShipOfPlayer2 = null;
	}
}
