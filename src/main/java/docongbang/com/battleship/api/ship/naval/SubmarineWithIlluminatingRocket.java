package docongbang.com.battleship.api.ship.naval;

import java.util.ArrayList;

import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.player.Player;
import docongbang.com.battleship.api.ship.naval.exceptions.OnlySubmarineHaveIlluminatingRocket;
import docongbang.com.battleship.api.utils.ShipOrientation;

public class SubmarineWithIlluminatingRocket extends NavalShipDecorator {
	private int numberOfRockets;

	/**
	 * Construct the upgraded submarine from the basic submarine.
	 * 
	 * @param ship
	 * @throws OnlySubmarineHaveIlluminatingRocket
	 */
	public SubmarineWithIlluminatingRocket(NavalShip ship) throws OnlySubmarineHaveIlluminatingRocket {
		super(ship);
		numberOfRockets = 1;
		if (ship instanceof Submarine == false) {
			throw new OnlySubmarineHaveIlluminatingRocket("This ship is not a submarine");
		}
	}

	/**
	 * Get number of rockets
	 * 
	 * @return
	 */
	public int getNumberOfRockets() {
		return numberOfRockets;
	}

	/**
	 * Set number of rockets
	 * 
	 * @param numberOfRockets
	 */
	public void setNumberOfRockets(int numberOfRockets) {
		this.numberOfRockets = numberOfRockets >= 0 ? numberOfRockets : 0;
	}

	@Override
	public int getLength() {
		return getDecoratedShip().getLength();
	}

	@Override
	public int getWidth() {
		return getDecoratedShip().getWidth();
	}

	@Override
	public String getName() {
		return getDecoratedShip().getName();
	}

	@Override
	public boolean isAlive() {
		return getDecoratedShip().isAlive();
	}

	@Override
	public int getHealth() {
		return getDecoratedShip().getHealth();
	}

	@Override
	public Player getCaptain() {
		return getDecoratedShip().getCaptain();
	}

	@Override
	public boolean wasPlacedOnGrid() {
		return getDecoratedShip().wasPlacedOnGrid();
	}

	@Override
	public void setLength(int length) {
		getDecoratedShip().setLength(length);
	}

	@Override
	public void setWidth(int width) {
		getDecoratedShip().setWidth(width);
	}

	@Override
	public void setCaptain(Player captain) {
		getDecoratedShip().setCaptain(captain);
	}

	@Override
	public void addCell(CellTrackingFleet cell) {
		getDecoratedShip().addCell(cell);
	}

	@Override
	public ArrayList<CellTrackingFleet> getCells() {
		return getDecoratedShip().getCells();
	}

	@Override
	public void attack(CellTrackingFleet target, int damage) {
		getDecoratedShip().attack(target, damage);
	}

	@Override
	public void upgrade(CellTrackingFleet cell) {
		getDecoratedShip().upgrade(cell);
	}

	@Override
	public ShipOrientation getOrientation() {
		return getDecoratedShip().getOrientation();
	}

	@Override
	public void setOrientation(ShipOrientation orientation) {
		getDecoratedShip().setOrientation(orientation);
	}

	@Override
	public void resetPosition() {
		getDecoratedShip().resetPosition();
	}

	@Override
	public CellTrackingFleet getOrigin() {
		return getDecoratedShip().getOrigin();
	}

	@Override
	public void setOrigin(CellTrackingFleet origin) {
		getDecoratedShip().setOrigin(origin);
	}
}
