package docongbang.com.battleship.api.ship.naval;

import docongbang.com.battleship.api.player.Player;

public class Submarine extends BasicNavalShip {
	private final static int WIDTH = 1;
	private final static int LENGTH = 3;

	public Submarine(Player captain) {
		super("Submarine", WIDTH, LENGTH, captain);
	}
}
