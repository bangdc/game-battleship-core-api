package docongbang.com.battleship.api.ship;

import java.util.ArrayList;

import docongbang.com.battleship.api.game.Game;
import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.player.Player;
import docongbang.com.battleship.api.utils.ShipOrientation;

/**
 * The BasicShip is a type of Ship with basic features.
 * 
 * @author bangdc
 * @since 29-12-2016
 * @see Ship
 */
public class BasicShip implements Ship {
	private String name;
	private int width;
	private int length;
	private CellTrackingFleet origin;
	private ShipOrientation orientation;
	private Player captain;
	private ArrayList<CellTrackingFleet> cells;

	/**
	 * Construct a new basic ship.
	 * 
	 * @param name
	 *            Name of the ship
	 * @param width
	 *            Width of the ship
	 * @param length
	 *            Length of the ship
	 */
	public BasicShip(final String name, final int width, final int length, final Player captain) {
		this.name = name;
		this.width = width;
		this.length = length;
		this.captain = captain;
		this.cells = new ArrayList<CellTrackingFleet>();
	}

	@Override
	public void addCell(final CellTrackingFleet cell) {
		if (!this.equals(cell.getShip())) {
			cell.setShip(this);
		}
		this.cells.add(cell);
	}

	@Override
	public ArrayList<CellTrackingFleet> getCells() {
		return cells;
	}

	@Override
	public void setCaptain(Player captain) {
		this.captain = captain;
	}

	@Override
	public Player getCaptain() {
		return captain;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getHealth() {
		int health = 0;
		for (CellTrackingFleet cell : cells) {
			health += cell.getHealth();
		}
		return health;
	}

	@Override
	public int getLength() {
		return length;
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public boolean isAlive() {
		return getHealth() > 0;
	}

	@Override
	public boolean wasPlacedOnGrid() {
		if (cells.size() == length * width) {
			for (CellTrackingFleet cell : cells) {
				if (cell.getShip().equals(this) != true) {
					return false;
				}
			}
		} else {
			return false;
		}
		return true;
	}

	@Override
	public void setWidth(int width) {
		this.width = width;
	}

	@Override
	public void setLength(int length) {
		this.length = length;
	}

	// @Override
	// public boolean equals(Object obj) {
	// if (obj == null) {
	// return false;
	// } else {
	// if (obj instanceof BasicShip) {
	// if (obj == this) {
	// return true;
	// } else {
	// BasicShip that = (BasicShip) obj;
	// if (that.name != this.name) {
	// return false;
	// }
	// if (that.width != this.width) {
	// return false;
	// }
	// if (that.length != this.length) {
	// return false;
	// }
	// if (!that.captain.equals(this.captain)) {
	// return false;
	// }
	// if (this.cells.size() != that.cells.size()) {
	// return false;
	// } else {
	// for (int i = 0; i < this.cells.size(); i++) {
	// CellTrackingFleet thisCell = this.cells.get(i);
	// CellTrackingFleet thatCell = that.cells.get(i);
	// if (!thisCell.equals(thatCell)) {
	// return false;
	// }
	// }
	// }
	// return true;
	// }
	// } else {
	// return false;
	// }
	// }
	// }
	//
	// @Override
	// public int hashCode() {
	// int hash = 17;
	// final int fact = 7;
	// hash += fact * hash + name.hashCode();
	// hash += fact * hash + width;
	// hash += fact * hash + length;
	// hash += fact * hash + captain.hashCode();
	// for (CellTrackingFleet cell : cells) {
	// hash += fact * hash + cell.hashCode();
	// }
	// return hash;
	// }

	@Override
	public void upgrade(CellTrackingFleet cell) {
		if (getCells().contains(cell)) {
			cell.setHealth(cell.getHealth() + Game.CELL_UNIT_HEALTH);
		}
	}

	@Override
	public void setOrigin(CellTrackingFleet origin) {
		this.origin = origin;
	}

	@Override
	public CellTrackingFleet getOrigin() {
		return origin;
	}

	@Override
	public ShipOrientation getOrientation() {
		return orientation;
	}

	@Override
	public void setOrientation(ShipOrientation orientation) {
		this.orientation = orientation;
	}

	@Override
	public void resetPosition() {
		orientation = null;
		origin = null;
		for (CellTrackingFleet cell : cells) {
			cell.setShip(null);
		}
		cells = new ArrayList<CellTrackingFleet>();
	}
}
