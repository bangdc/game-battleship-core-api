package docongbang.com.battleship.api.ship.naval.exceptions;

public class FactoryCouldNotProduceTheShip extends Exception {

	public FactoryCouldNotProduceTheShip(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1706707179596189076L;
}
