package docongbang.com.battleship.api.ship.naval.behaviors.attack;

import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import docongbang.com.battleship.api.game.Game;
import docongbang.com.battleship.api.grid.cell.CellBomb;
import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.player.Player;
import docongbang.com.battleship.api.ship.naval.NavalShip;
import docongbang.com.battleship.api.ship.naval.exceptions.RunOutOfRocket;

/**
 * A concrete attack strategy for the ship. This attack strategy will cause
 * damage only for the target cell.
 * 
 * @author bangdc
 * @since 29-12-2016
 */
public class AttackClassic implements AttackStrategy {
	private static final Logger LOG = LogManager.getLogger(AttackClassic.class);
	private NavalShip dutyShip;

	/**
	 * Construct the strategy
	 * 
	 * @param dutyShip
	 *            The ship carrying out the duty
	 */
	public AttackClassic(NavalShip dutyShip) {
		this.dutyShip = dutyShip;
	}

	@Override
	public void doAttack(CellTrackingFleet target, int damage) throws RunOutOfRocket {
		final Player captain = dutyShip.getCaptain();
		final Player opponent = captain.getOpponent();

		boolean causedCivilShipDestroyed = opponent.getSailingShip().getHealth() > 0;

		if (Game.DEBUGGABLE) {
			LOG.info(captain.getName() + " is attacking " + opponent.getName() + " by classic with ship "
					+ dutyShip.getName() + " at position " + target.toString());
		}

		target.setNumberOfTimesBeingAttacked(target.getNumberOfTimesBeingAttacked() + 1);
		if (target instanceof CellBomb) {
			CellBomb targetWithBomb = (CellBomb) target;

			if (Game.DEBUGGABLE) {
				LOG.info(captain.getName() + " TRIGGERED THE BOMB of " + opponent.getName());
			}
			if (targetWithBomb.wasTriggered()) {
				if (Game.DEBUGGABLE) {
					LOG.info("The bomb was already triggered, then treat being attacked as normal cell.");
				}
			} else {
				Hashtable<Integer, ArrayList<CellTrackingFleet>> surroundingCells = opponent.getGridTrackingFleet()
						.getSurroundingCells(target, Game.BOMB_IMPACT_LEVEL);
				for (Integer surroundingLevel : surroundingCells.keySet()) {
					for (CellTrackingFleet cell : surroundingCells.get(new Integer(surroundingLevel))) {
						cell.setNumberOfTimesBeingAttacked(cell.getNumberOfTimesBeingAttacked() + 1);
						cell.setHealth(cell.getHealth() - (Game.BOMB_DAMAGE - surroundingLevel.intValue()));
					}
				}
				target.setHealth(target.getHealth() - Game.BOMB_DAMAGE);
				targetWithBomb.setTriggered();
			}
		} else {
			target.setHealth(target.getHealth() - damage);
		}
		causedCivilShipDestroyed = causedCivilShipDestroyed && opponent.getSailingShip().getHealth() == 0;
		if (causedCivilShipDestroyed) {
			if (Game.DEBUGGABLE) {
				LOG.info(captain.getName() + " caused the DESTROY OF SAILING SHIP on grid of " + opponent.getName());
			}
			captain.setNumberOfTurnsRemaining(captain.getNumberOfTurnsRemaining() - Game.COST_DESTROYING_CIVIL_SHIP);
			opponent.setNumberOfTurnsRemaining(opponent.getNumberOfTurnsRemaining() + Game.COST_DESTROYING_CIVIL_SHIP);
			if (Game.DEBUGGABLE) {
				LOG.info(captain.getName() + " then will have number of turns remaining: "
						+ captain.getNumberOfTurnsRemaining());
			}
		}

		captain.setNumberOfTurnsRemaining(captain.getNumberOfTurnsRemaining() - Game.COST_ATTACKING_CLASSIC);
		opponent.setNumberOfTurnsRemaining(opponent.getNumberOfTurnsRemaining() + Game.COST_ATTACKING_CLASSIC);
	}
}
