package docongbang.com.battleship.api.game;

import java.util.Hashtable;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import docongbang.com.battleship.api.grid.GridTrackingFleet;
import docongbang.com.battleship.api.grid.cell.CellBomb;
import docongbang.com.battleship.api.grid.cell.CellTrackingAttack;
import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.player.Player;
import docongbang.com.battleship.api.player.PlayerComputer;
import docongbang.com.battleship.api.ship.civil.SailingShip;
import docongbang.com.battleship.api.utils.DifficultyLevel;
import docongbang.com.battleship.api.utils.GamePlayerMode;
import docongbang.com.battleship.api.utils.Helper;
import docongbang.com.battleship.api.utils.ShipOrientation;

/**
 * Battleship game
 * 
 * @author bangdc
 * @since 01-01-2017
 */
public class Game {
	private static final Logger LOG = LogManager.getLogger(Game.class);
	public static final boolean DEBUGGABLE = false;
	public static final int GRID_WIDTH = 10;
	public static final int GRID_HEIGHT = 10;
	public static final int NUMBER_CONSECUTIVE_FAILURES_TO_UPGRADE_SHIP = 3;
	public static final int NUMBER_CONSECUTIVE_FAILURES_TO_UNLOCK_ROCKET = 4;
	public static final int TURN_TIMING_MINUTE = 1;
	public static final String DEFAULT_COMPUTER_NAME = "General Computer";
	public static final int TIME_CONSTRAINT_EACH_TURN = TURN_TIMING_MINUTE * 60 * 1000;
	public static final int CELL_UNIT_HEALTH = 1;
	public static final int BOMB_IMPACT_LEVEL = 2;
	public static final int BOMB_DAMAGE = 3;
	public static final int COST_DESTROYING_CIVIL_SHIP = 3;
	public static final int COST_ATTACKING_CLASSIC = 1;
	public static final int COST_ATTACKING_CROSS = 2;
	public static final int COST_USING_ROCKET = 5;

	private GamePlayerMode mode;
	private DifficultyLevel difficulty;
	private Player player1;
	private Player player2;
	private Player currentPlayer;
	private Player winner;
	private Player loser;

	/**
	 * Game construction in mode computer plays with computer.
	 */
	public Game() {
		player1 = new PlayerComputer(DEFAULT_COMPUTER_NAME + "1", GRID_WIDTH, GRID_HEIGHT, this);
		player2 = new PlayerComputer(DEFAULT_COMPUTER_NAME + "2", GRID_WIDTH, GRID_HEIGHT, this);

		setMode(GamePlayerMode.COMPUTER_COMPUTER);
	}

	/**
	 * Game construction in mode player plays with computer.
	 * 
	 * @param player1Name
	 *            Player will be referenced by player1.
	 * @param difficulty
	 *            The difficulty of game.
	 * @see DifficultyLevel
	 */
	public Game(final String player1Name, DifficultyLevel difficulty) {
		player1 = new Player(player1Name, GRID_WIDTH, GRID_HEIGHT, this);
		player2 = new PlayerComputer(DEFAULT_COMPUTER_NAME, GRID_WIDTH, GRID_HEIGHT, this);

		setMode(GamePlayerMode.PLAYER_COMPUTER);
		setDifficulty(difficulty);
	}

	/**
	 * Game construction in mode player plays with other player.
	 * 
	 * @param player1Name
	 *            Name of player 1
	 * @param player2Name
	 *            Name of player 2
	 */
	public Game(final String player1Name, final String player2Name) {
		player1 = new Player(player1Name, GRID_WIDTH, GRID_HEIGHT, this);
		player2 = new Player(player2Name, GRID_WIDTH, GRID_HEIGHT, this);

		difficulty = DifficultyLevel.NA;
		mode = GamePlayerMode.PLAYER_PLAYER;
	}

	/**
	 * Each attacking turn of game belongs to only one player. Set the player
	 * who owns a turn in game. Must be one of 2 player
	 * 
	 * @param currentPlayer
	 */
	public void setCurrentPlayer(Player currentPlayer) {
		if (currentPlayer == player1 || currentPlayer == player2) {
			this.currentPlayer = currentPlayer;
		}
	}

	/**
	 * At the end of game, set the loser of game. Must be one of 2 player
	 * 
	 * @param loser
	 */
	public void setLoser(Player loser) {
		if (loser == player1 || loser == player2) {
			this.loser = loser;
		}
	}

	/**
	 * At the end of game, set the winner of game. Must be one of 2 player
	 * 
	 * @param winner
	 */
	public void setWinner(Player winner) {
		if (winner == player1 || winner == player2) {
			this.winner = winner;
		}
	}

	/**
	 * Finish a game after determining who is winner and who is loser.
	 * 
	 * @param winner
	 *            The winner of game.
	 * @param loser
	 *            The loser of game.
	 */
	public void finish(final Player winner, final Player loser) {
		setWinner(winner);
		setLoser(loser);
	}

	/**
	 * Initialize the bomb and sailing ship on the grid of each user
	 */
	public void initBombAndSailingShip() {
		final Random random = new Random();
		// For player1
		final int cellBombColIndexPlayer1 = random.nextInt(GRID_WIDTH);
		final int cellBombRowIndexPlayer1 = random.nextInt(GRID_HEIGHT);
		final Integer cellBombIndexPlayer1 = Integer
				.valueOf(Helper.calculatePairingNumber(cellBombColIndexPlayer1, cellBombRowIndexPlayer1));
		final GridTrackingFleet gridFleetPlayer1 = player1.getGridTrackingFleet();
		CellTrackingFleet cellForBomb1 = gridFleetPlayer1.getCells().get(cellBombIndexPlayer1);
		if (cellForBomb1 == null) {
			// If the cell not yet created
			gridFleetPlayer1.getCells().put(cellBombIndexPlayer1,
					CellBomb.getCellOnPlayer1Grid(cellBombColIndexPlayer1, cellBombRowIndexPlayer1));
		} else {
			// If the cell already existed, need to transform to CellBomb
			gridFleetPlayer1.getCells().put(cellBombIndexPlayer1, CellBomb.getCellOnPlayer1Grid(cellForBomb1));
		}

		if (DEBUGGABLE) {
			LOG.info("The bomb is at " + gridFleetPlayer1.getCells().get(cellBombIndexPlayer1) + " on grid of "
					+ player1.getName());
		}
		while (true) {
			final char sailingColIndex = (char) (random.nextInt(GRID_WIDTH) + 'A');
			final int sailingRowIndex = random.nextInt(GRID_HEIGHT) + 1;
			if (gridFleetPlayer1.addShip(SailingShip.getSailingShipOfPlayer1(player1), sailingColIndex, sailingRowIndex,
					ShipOrientation.HORIZONTAL)) {
				if (DEBUGGABLE) {
					LOG.info("The sailing ship is placed at (" + sailingColIndex + "," + sailingRowIndex
							+ ") on grid of " + player1.getName());
				}
				SailingShip.getSailingShipOfPlayer1(player1).initHealth();
				player1.setSailingShip(SailingShip.getSailingShipOfPlayer1(player1));
				break;
			}
		}

		// For player2
		final int cellBombColIndexPlayer2 = random.nextInt(GRID_WIDTH);
		final int cellBombRowIndexPlayer2 = random.nextInt(GRID_HEIGHT);
		final Integer cellBombIndexPlayer2 = Integer
				.valueOf(Helper.calculatePairingNumber(cellBombColIndexPlayer2, cellBombRowIndexPlayer2));
		final GridTrackingFleet gridFleetPlayer2 = player2.getGridTrackingFleet();
		CellTrackingFleet cellForBomb2 = gridFleetPlayer2.getCells().get(cellBombIndexPlayer2);
		if (cellForBomb2 == null) {
			// If the cell not yet created
			gridFleetPlayer2.getCells().put(cellBombIndexPlayer2,
					CellBomb.getCellOnPlayer2Grid(cellBombColIndexPlayer2, cellBombRowIndexPlayer2));
		} else {
			// If the cell already existed, need to transform to CellBomb
			gridFleetPlayer2.getCells().put(cellBombIndexPlayer2, CellBomb.getCellOnPlayer2Grid(cellForBomb2));
		}
		if (DEBUGGABLE) {
			LOG.info("The bomb is at " + gridFleetPlayer2.getCells().get(cellBombIndexPlayer2) + " on grid of "
					+ player2.getName());
		}
		while (true) {
			final char sailingColIndex = (char) (random.nextInt(GRID_WIDTH) + 'A');
			final int sailingRowIndex = random.nextInt(GRID_HEIGHT) + 1;
			if (gridFleetPlayer2.addShip(SailingShip.getSailingShipOnPlayer2(player2), sailingColIndex, sailingRowIndex,
					ShipOrientation.HORIZONTAL)) {
				if (DEBUGGABLE) {
					LOG.info("The sailing ship is placed at (" + sailingColIndex + "," + sailingRowIndex
							+ ") on grid of " + player2.getName());
				}
				SailingShip.getSailingShipOnPlayer2(player2).initHealth();
				player2.setSailingShip(SailingShip.getSailingShipOnPlayer2(player2));
				break;
			}
		}

	}

	/**
	 * Place automatically ships on the grid of both users
	 */
	public void placeShipsAuto() {
		player1.placeShipsAuto();
		player2.placeShipsAuto();
	}

	/**
	 * Update the grid of each user after each attacking turn.
	 */
	public void updateAfterEachTurn() {
		final Player currentPlayer = getCurrentPlayer();
		final Player opponent = currentPlayer.getOpponent();
		Hashtable<Integer, CellTrackingFleet> opponentCellsTrackingFleet = opponent.getGridTrackingFleet().getCells();
		Hashtable<Integer, CellTrackingAttack> playerCellsTrackingResult = currentPlayer.getGridTrackingAttackResult()
				.getCells();
		for (Integer cellIndex : opponentCellsTrackingFleet.keySet()) {
			playerCellsTrackingResult.get(cellIndex).setNumberOfTimesBeingAttacked(
					opponentCellsTrackingFleet.get(cellIndex).getNumberOfTimesBeingAttacked());
			if (opponentCellsTrackingFleet.get(cellIndex).wasAttacked()) {
				if (opponentCellsTrackingFleet.get(cellIndex).doesHaveNavalShip()) {
					playerCellsTrackingResult.get(cellIndex).setHasShip(true);
				}
			}
			if (opponentCellsTrackingFleet.get(cellIndex).isVisible()) {
				playerCellsTrackingResult.get(cellIndex).setVisible();
				if (opponentCellsTrackingFleet.get(cellIndex).doesHaveNavalShip()) {
					playerCellsTrackingResult.get(cellIndex).setHasShip(true);
				}
			}
		}
	}

	/**
	 * @return Mode of game.
	 * @see GamePlayerMode
	 */
	public GamePlayerMode getMode() {
		return mode;
	}

	/**
	 * @return The difficulty level of game.
	 * @see DifficultyLevel
	 */
	public DifficultyLevel getDifficulty() {
		return difficulty;
	}

	/**
	 * Set the difficulty level of game.
	 * 
	 * @param difficulty
	 * @see DifficultyLevel
	 */
	public void setDifficulty(DifficultyLevel difficulty) {
		this.difficulty = difficulty;
		// checkInvariants();
	}

	/**
	 * Set the mode of game
	 * 
	 * @param mode
	 * @see GamePlayerMode
	 */
	public void setMode(GamePlayerMode mode) {
		this.mode = mode;
	}

	/**
	 * @return The loser of game.
	 */
	public Player getLoser() {
		return loser;
	}

	/**
	 * @return The winner of game.
	 */
	public Player getWinner() {
		return winner;
	}

	/**
	 * @return Player 1 of game
	 */
	public Player getPlayer1() {
		return player1;
	}

	/**
	 * @return Player 2 of game
	 */
	public Player getPlayer2() {
		return player2;
	}

	/**
	 * @return The current player of game
	 */
	public Player getCurrentPlayer() {
		return currentPlayer;
	}

	/**
	 * Reset the CellBomb and SailingShip class, so we can create a new game
	 */
	public void reset() {
		CellBomb.reset();
		SailingShip.reset();
	}
}
