package docongbang.com.battleship.api.grid.cell;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The CellBomb is a kind of CellTrackingFleet which has a bomb under it. The
 * CellBomb might have a Ship on it.
 * 
 * @author bangdc
 *
 */
public class CellBomb extends CellTrackingFleet {
	private static final Logger LOG = LogManager.getLogger(CellBomb.class);
	private static CellBomb cellOnPlayer1Grid;
	private static CellBomb cellOnPlayer2Grid;
	private boolean triggered = false;

	/**
	 * Private constructor for CellWithBomb We want to have ONLY 2 instances of
	 * this class. One for each player.
	 * 
	 * @param colIndex
	 * @param rowIndex
	 */
	private CellBomb(int colIndex, int rowIndex) {
		super(colIndex, rowIndex);
	}

	public static boolean wasBombPlayer1Triggered() {
		if (cellOnPlayer1Grid != null) {
			return cellOnPlayer1Grid.wasTriggered();
		}
		return false;
	}

	public static boolean wasBombPlayer2Triggered() {
		if (cellOnPlayer2Grid != null) {
			return cellOnPlayer2Grid.wasTriggered();
		}
		return false;
	}

	/**
	 * Static method to get the cell for player 1. If it is NULL, it will be
	 * created by using private construction above. If it was already created,
	 * we need to check the correspondence of column and row before returning
	 * 
	 * @param colIndex
	 * @param rowIndex
	 * @param grid
	 * @return
	 */
	public static CellBomb getCellOnPlayer1Grid(final int colIndex, final int rowIndex) {
		if (cellOnPlayer1Grid == null) {
			cellOnPlayer1Grid = new CellBomb(colIndex, rowIndex);
		}
		if (cellOnPlayer1Grid.getColIndex() != colIndex || cellOnPlayer1Grid.getRowIndex() != rowIndex) {
			return null;
		} else {
			return cellOnPlayer1Grid;
		}
	}

	/**
	 * Upgraded a CellTrackingFleet to a CellBomb. This bomb is placed on the
	 * grid of player 1
	 * 
	 * @param cell
	 * @return
	 */
	public static CellBomb getCellOnPlayer1Grid(CellTrackingFleet cell) {
		cellOnPlayer1Grid = new CellBomb(cell.getColIndex(), cell.getRowIndex());
		if (cell.doesHaveShip()) {
			cellOnPlayer1Grid.setHasShip(true);
			cellOnPlayer1Grid.setHealth(cell.getHealth());
			cellOnPlayer1Grid.setShip(cell.getShip());
			for (int i = 0; i < cell.getShip().getCells().size(); i++) {
				if (cell.getShip().getCells().get(i) == cell) {
					cell.getShip().getCells().set(i, cellOnPlayer1Grid);
					break;
				}
			}
		}
		return cellOnPlayer1Grid;
	}

	/**
	 * Static method to get the cell for player 2. If it is NULL, it will be
	 * created by using private construction above. If it was already created,
	 * we need to check the correspondence of column and row before returning
	 * 
	 * @param colIndex
	 * @param rowIndex
	 * @param grid
	 * @return
	 */
	public static CellBomb getCellOnPlayer2Grid(final int colIndex, final int rowIndex) {
		if (cellOnPlayer2Grid == null) {
			cellOnPlayer2Grid = new CellBomb(colIndex, rowIndex);
		}
		if (cellOnPlayer2Grid.getColIndex() != colIndex || cellOnPlayer2Grid.getRowIndex() != rowIndex) {
			return null;
		} else {
			return cellOnPlayer2Grid;
		}
	}

	/**
	 * Upgraded a CellTrackingFleet of player2 to a CellBomb
	 * 
	 * @param cell
	 * @return
	 */
	public static CellBomb getCellOnPlayer2Grid(final CellTrackingFleet cell) {
		cellOnPlayer2Grid = new CellBomb(cell.getColIndex(), cell.getRowIndex());
		if (cell.doesHaveShip()) {
			cellOnPlayer2Grid.setHasShip(true);
			cellOnPlayer2Grid.setHealth(cell.getHealth());
			cellOnPlayer2Grid.setShip(cell.getShip());
			for (int i = 0; i < cell.getShip().getCells().size(); i++) {
				if (cell.getShip().getCells().get(i) == cell) {
					cell.getShip().getCells().set(i, cellOnPlayer2Grid);
					break;
				}
			}
		}
		return cellOnPlayer2Grid;
	}

	/**
	 * This cell will always have a bomb under water.
	 */
	@Override
	public boolean doesHaveBomb() {
		return true;
	}

	/**
	 * Set the bomb as triggered
	 */
	public void setTriggered() {
		triggered = true;
		if (this == cellOnPlayer1Grid) {
			LOG.info("Bomb on the grid of player 1 was triggered");
		}
		if (this == cellOnPlayer2Grid) {
			LOG.info("Bomb on the grid of player 2 was triggered");
		}
	}

	/**
	 * Check if the bomb was triggered
	 * 
	 * @return
	 */
	public boolean wasTriggered() {
		return triggered;
	}

	/**
	 * Reset the class. Make it ready for a new game.
	 */
	public static void reset() {
		cellOnPlayer1Grid = null;
		cellOnPlayer2Grid = null;
	}
}
