package docongbang.com.battleship.api.grid;

import java.util.ArrayList;
import java.util.Hashtable;

import docongbang.com.battleship.api.grid.cell.CellTrackingAttack;
import docongbang.com.battleship.api.utils.Helper;

/**
 * This is a grid for tracking attack result of each player.
 * 
 * @author bangdc
 * @since 29-12-2016
 */
public class GridTrackingAttack extends Grid {
	private final Hashtable<Integer, CellTrackingAttack> cells;

	/**
	 * Construct a new grid
	 * 
	 * @param width
	 *            The width of the grid
	 * @param height
	 *            The height of the grid
	 */
	public GridTrackingAttack(int width, int height) {
		super(width, height);
		this.cells = new Hashtable<Integer, CellTrackingAttack>();
		for (int col = 0; col < width; col++) {
			for (int row = 0; row < height; row++) {
				cells.put(Helper.calculatePairingNumber(col, row), new CellTrackingAttack(col, row));
			}
		}
	}

	/**
	 * Get cells of grid
	 * 
	 * @return
	 */
	public Hashtable<Integer, CellTrackingAttack> getCells() {
		return cells;
	}

	/**
	 * Get all cells index in the grid. Each cell index is unique and help to
	 * locate a cell quickly in the grid.
	 * 
	 * @return The list of all cells index
	 * @see Helper
	 */
	public ArrayList<Integer> getCellsIndex() {
		ArrayList<Integer> result = new ArrayList<Integer>();
		for (Integer cellIndex : getCells().keySet()) {
			result.add(cellIndex);
		}
		return result;
	}

	/**
	 * Get the text presentation of the grid. A cell could be presented as:
	 * <ul>
	 * <li>|3*| if it has ship and it was attacked 3 times.</li>
	 * <li>|x | if it DOES NOT have ship on it and it was attacked.</li>
	 * <li>| | if it has not been yet attacked.</li>
	 * </ul>
	 * 
	 * A sample of presentation could be:
	 * <p>
	 * |__|A|B|C|D|
	 * </p>
	 * <p>
	 * | 1|_ |__|_|_|
	 * </p>
	 * <p>
	 * | 2|3*|__|_|_|
	 * </p>
	 * <p>
	 * | 3|x |__|_|x|
	 * </p>
	 * <p>
	 * | 4|_ |2*|_|_|
	 * </p>
	 */
	@Override
	public String toString() {
		StringBuilder gridPrint = new StringBuilder("|" + String.format(Grid.CELL_SPACE, "") + "|");
		for (int i = 0; i < getWidth(); i++) {
			gridPrint.append(String.format(Grid.CELL_SPACE, (char) ((int) 'A' + i)) + "|");
		}
		gridPrint.append("\n");
		for (int rowIndex = 0; rowIndex < getHeight(); rowIndex++) {
			gridPrint.append("|" + String.format(Grid.CELL_SPACE, (rowIndex + 1)) + "|");
			for (int colIndex = 0; colIndex < getWidth(); colIndex++) {
				final int cellIndex = Helper.calculatePairingNumber(colIndex, rowIndex);
				CellTrackingAttack cell = getCells().get(cellIndex);
				if (cell.wasAttacked()) {
					if (cell.doesHaveShip()) {
						gridPrint.append(
								String.format(Grid.CELL_SPACE, cell.getNumberOfTimesBeingAttacked() + "*") + "|");
					} else {
						gridPrint.append(String.format(Grid.CELL_SPACE, "x") + "|");
					}
				} else {
					gridPrint.append(String.format(Grid.CELL_SPACE, "") + "|");
				}
			}
			gridPrint.append("\n");
		}
		return gridPrint.toString();
	}

	// @Override
	// public boolean equals(Object obj) {
	// if (super.equals(obj)) {
	// GridTrackingAttack that = (GridTrackingAttack) obj;
	// for (int col = 0; col < getWidth(); col++) {
	// for (int row = 0; row < getHeight(); row++) {
	// int cellIndex = Helper.calculatePairingNumber(col, row);
	// CellTrackingAttack thisCell = this.cells.get(cellIndex);
	// CellTrackingAttack thatCell = that.cells.get(cellIndex);
	// if (!thisCell.equals(thatCell)) {
	// return false;
	// }
	// }
	// }
	// return true;
	// } else {
	// return false;
	// }
	// }
	//
	// @Override
	// public int hashCode() {
	// int hash = super.hashCode();
	// final int fact = 7;
	// for (int col = 0; col < getWidth(); col++) {
	// for (int row = 0; row < getHeight(); row++) {
	// int cellIndex = Helper.calculatePairingNumber(col, row);
	// hash += hash * fact + this.cells.get(cellIndex).hashCode();
	// }
	// }
	// return hash;
	// }
}
