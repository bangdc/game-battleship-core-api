package docongbang.com.battleship.api.grid;

import docongbang.com.battleship.api.game.Game;

/**
 * The Grid is characterized by width (horizontal) and height (vertical).
 * 
 * @author bangdc
 * @since 29-12-2016
 */
public abstract class Grid {
	public final static String CELL_SPACE = "%3s";
	private final int width;
	private final int height;
	// Invariants:
	// width >= 1
	// height >= 1

	// Safety from exposure:
	// fields are primitive

	private void checkInvariants() {
		assert width > 0;
		assert height > 0;
	}

	/**
	 * Construct a new grid.
	 * 
	 * @param width
	 *            The width of the grid.
	 * @param height
	 *            The height of the grid.
	 */
	public Grid(final int width, final int height) {
		this.width = width;
		this.height = height;
		if (Game.DEBUGGABLE) {
			checkInvariants();
		}
	}

	/**
	 * @return width of the grid
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @return height of the grid
	 */
	public int getHeight() {
		return height;
	}
}