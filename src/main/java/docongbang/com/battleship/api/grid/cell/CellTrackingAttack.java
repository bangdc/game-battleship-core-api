package docongbang.com.battleship.api.grid.cell;

/**
 * The CellTrackingAttack represents a cell on the GridTrackingAttack
 * 
 * @author bangdc
 *
 */
public class CellTrackingAttack extends Cell {
	/**
	 * Construct a new cell
	 * 
	 * @param colIndex
	 * @param rowIndex
	 */
	public CellTrackingAttack(int colIndex, int rowIndex) {
		super(colIndex, rowIndex);
	}
}
