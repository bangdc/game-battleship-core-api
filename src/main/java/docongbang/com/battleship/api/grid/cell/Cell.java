package docongbang.com.battleship.api.grid.cell;

/**
 * The Cell is basic element of a grid. It is located by the row index and
 * column index on the grid. The Column Index starts from 0 (i.e A). 1 (i.e B)
 * The Row Index starts from 0.
 * 
 * @author bangdc
 * @since 29-12-2016
 */
public class Cell {
	private int rowIndex;
	private int colIndex;
	private int numberOfTimesBeingAttacked;
	private boolean hasShip;
	private boolean visible;
	// Invariants:
	// rowIndex >= 0
	// colIndex >= 0
	// numberOfTimesBeingAttacked >= 0

	/**
	 * Construct a new cell at the column index and row index.
	 * 
	 * @param colIndex
	 * @param rowIndex
	 */
	public Cell(final int colIndex, final int rowIndex) {
		this.rowIndex = rowIndex;
		this.colIndex = colIndex;
		this.numberOfTimesBeingAttacked = 0;
		this.hasShip = false;
		this.visible = false;
	}

	/**
	 * @return The number of times for that this cell was be attacked.
	 */
	public int getNumberOfTimesBeingAttacked() {
		return numberOfTimesBeingAttacked;
	}

	/**
	 * @return The row index of the cell on the grid
	 */
	public int getRowIndex() {
		return rowIndex;
	}

	/**
	 * @return The column index of the cell on the grid
	 */
	public int getColIndex() {
		return colIndex;
	}

	/**
	 * The character of column
	 * 
	 * @return
	 */
	public char getCol() {
		return (char) (colIndex + (int) 'A');
	}

	/**
	 * The number of row
	 * 
	 * @return
	 */
	public int getRow() {
		return rowIndex + 1;
	}

	/**
	 * For verifying if the cell is used by a ship.
	 * 
	 * @param hasShip
	 */
	public void setHasShip(boolean hasShip) {
		this.hasShip = hasShip;
	}

	/**
	 * @return Check whether the cell was bombed or not yet.
	 */
	public boolean wasAttacked() {
		return numberOfTimesBeingAttacked > 0;
	}

	/**
	 * @return Check whether this cell is used by a ship
	 */
	public boolean doesHaveShip() {
		return hasShip;
	}

	/**
	 * @return By default a cell does not have bomb under-water, except when the
	 *         cell is an instance of CellBomb.
	 */
	public boolean doesHaveBomb() {
		return false;
	}

	/**
	 * Check whether the cell is visible or not.
	 * 
	 * @return true or false
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * Make the cell to be visible for opponent
	 */
	public void setVisible() {
		visible = true;
	}

	/**
	 * Generate the coordinate of cell as String.
	 */
	@Override
	public String toString() {
		return "(" + (char) (colIndex + 'A') + "," + (rowIndex + 1) + ")";
	}

	/**
	 * Set the number of times being attacked of this cell
	 * 
	 * @param numberOfTimesBeingAttacked
	 */
	public void setNumberOfTimesBeingAttacked(int numberOfTimesBeingAttacked) {
		this.numberOfTimesBeingAttacked = numberOfTimesBeingAttacked;
	}
}