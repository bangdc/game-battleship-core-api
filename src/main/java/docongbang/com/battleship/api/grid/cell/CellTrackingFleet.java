package docongbang.com.battleship.api.grid.cell;

import docongbang.com.battleship.api.ship.Ship;
import docongbang.com.battleship.api.ship.civil.CivilShip;
import docongbang.com.battleship.api.ship.naval.NavalShip;

/**
 * The CellTrackingFleet is basic element of a GridTrackingFleet It is located
 * by the row index and column index on the grid. The Column Index starts from 0
 * (i.e A). 1 (i.e B) The Row Index starts from 0. The CellTrackingFleet can
 * have reference to a ship.
 * 
 * @author bangdc
 * @since 29-12-2016
 */
public class CellTrackingFleet extends Cell {
	private Ship ship;
	private int health;

	/**
	 * Construct a new cell at the column index and row index.
	 * 
	 * @param colIndex
	 * @param rowIndex
	 */
	public CellTrackingFleet(final int colIndex, final int rowIndex) {
		super(colIndex, rowIndex);
		this.health = 0;
	}

	/**
	 * @return Health, or capability of defense, resistance of this cell
	 */
	public int getHealth() {
		return health;
	}

	/**
	 * Set the health of this cell
	 * 
	 * @param health
	 *            The amount to increase
	 */
	public void setHealth(int health) {
		if (health < 0) {
			this.health = 0;
		} else {
			this.health = health;
		}
	}

	/**
	 * @return the ship that is on this cell
	 */
	public Ship getShip() {
		return ship;
	}

	/**
	 * Set the ship that is on this cell. One cell can be used by only one ship.
	 * If the cell has a ship, the attribute hasShip must be true. The inverse
	 * is not necessary enforced.
	 * 
	 * @param ship
	 */
	public void setShip(Ship ship) {
		this.ship = ship;
		if (ship != null) {
			setHasShip(true);
		} else {
			setHasShip(false);
		}
	}

	/**
	 * @return Check whether this cell is used by a civil ship
	 */
	public boolean doesHaveCivilShip() {
		if (doesHaveShip() && (getShip() instanceof CivilShip)) {
			return true;
		}
		return false;
	}

	/**
	 * @return Check whether this cell is used by a naval ship
	 */
	public boolean doesHaveNavalShip() {
		if (doesHaveShip() && (getShip() instanceof NavalShip)) {
			return true;
		}
		return false;
	}
}