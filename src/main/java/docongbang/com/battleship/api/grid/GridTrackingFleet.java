package docongbang.com.battleship.api.grid;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;

import docongbang.com.battleship.api.game.Game;
import docongbang.com.battleship.api.grid.cell.Cell;
import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.ship.Ship;
import docongbang.com.battleship.api.utils.Helper;
import docongbang.com.battleship.api.utils.ShipOrientation;

/**
 * A grid for tracking all ships of player.
 * 
 * @author bangdc
 * @since 29-12-2016
 */
public class GridTrackingFleet extends Grid {
	private final Hashtable<Integer, CellTrackingFleet> cells;

	/**
	 * Construct a new grid
	 * 
	 * @param width
	 * @param height
	 */
	public GridTrackingFleet(int width, int height) {
		super(width, height);
		this.cells = new Hashtable<Integer, CellTrackingFleet>();
		for (int col = 0; col < width; col++) {
			for (int row = 0; row < height; row++) {
				cells.put(Helper.calculatePairingNumber(col, row), new CellTrackingFleet(col, row));
			}
		}
	}

	/**
	 * TODO problem with mutability. Should make defensive copy of cells before
	 * returning.
	 * 
	 * @return all cells of the grid
	 */
	public Hashtable<Integer, CellTrackingFleet> getCells() {
		return cells;
	}

	/**
	 * Get all cells index in the grid. Each cell index is unique and help to
	 * locate a cell quickly in the grid.
	 * 
	 * @return The list of all cells index
	 * @see Helper
	 */
	public ArrayList<Integer> getCellsIndex() {
		ArrayList<Integer> result = new ArrayList<Integer>();
		for (Integer cellIndex : getCells().keySet()) {
			result.add(cellIndex);
		}
		return result;
	}

	/**
	 * Get all the cells that surround a center cell, by a level. For example,
	 * if a surrounding level is 1, the number of surrounding cells can be up to
	 * 8 cells. If a surrounding level is 2, the number of surrounding cells can
	 * be up to 24 cells. ( 8 of level 1, and 16 of level 2). Pre-conditions: -
	 * surroundingLevel > 0 - centerCell not null Post-conditions: - Not include
	 * centerCell
	 * 
	 * @param centerCell
	 * @param surroundingLevel
	 * @return
	 */
	public Hashtable<Integer, ArrayList<CellTrackingFleet>> getSurroundingCells(final Cell centerCell,
			int surroundingLevel) {
		assert surroundingLevel > 0;
		assert centerCell.getColIndex() > -1;
		assert centerCell.getRowIndex() > -1;

		final int centerCellColIndex = centerCell.getColIndex();
		final int centerCellRowIndex = centerCell.getRowIndex();

		Hashtable<Integer, ArrayList<CellTrackingFleet>> result = new Hashtable<Integer, ArrayList<CellTrackingFleet>>();
		while (surroundingLevel > 0) {
			int currentLevelTemp = surroundingLevel;
			HashSet<Integer> possibleColIndex = new HashSet<Integer>();
			HashSet<Integer> possibleRowIndex = new HashSet<Integer>();
			while (currentLevelTemp > -1) {
				possibleColIndex.add(new Integer(centerCellColIndex + currentLevelTemp));
				possibleRowIndex.add(new Integer(centerCellRowIndex + currentLevelTemp));
				if (centerCellColIndex - currentLevelTemp > -1) {
					possibleColIndex.add(new Integer(centerCellColIndex - currentLevelTemp));
				}
				if (centerCellRowIndex - currentLevelTemp > -1) {
					possibleRowIndex.add(new Integer(centerCellRowIndex - currentLevelTemp));
				}
				currentLevelTemp--;
			}
			HashSet<Integer> possibleCellIndex = new HashSet<Integer>();
			for (Integer col : possibleColIndex) {
				for (Integer row : possibleRowIndex) {
					if (Math.abs(col - centerCellColIndex) >= surroundingLevel
							|| Math.abs(row - centerCellRowIndex) >= surroundingLevel) {
						possibleCellIndex.add(Helper.calculatePairingNumber(col.intValue(), row.intValue()));
					}
				}
			}

			ArrayList<CellTrackingFleet> cells = new ArrayList<CellTrackingFleet>();
			for (Integer index : possibleCellIndex) {
				if (getCells().get(index) != null) {
					cells.add(getCells().get(index));
				}
			}
			result.put(new Integer(surroundingLevel), cells);
			surroundingLevel--;
		}
		return result;
	}

	/**
	 * Add a ship to the grid. The range corresponding to the dimension of the
	 * ship must be available on the grid. That means the range have not been
	 * yet acquired by other ship. Increase by 1 the health of all cells
	 * acquired by this ship. Add this cell to the list of cells of this ship.
	 * 
	 * if the ship did already have cells, that means player is changing
	 * position of that ship.
	 * 
	 * @param ship
	 * @param col
	 *            The most upper-left position, column index, of the ship. Start
	 *            from A.
	 * @param row
	 *            The most upper-left position, row index, of the ship. Start
	 *            from 1.
	 * @param orientation
	 *            VERTICAL or HORIZONTAL.
	 * @return true if success, false otherwise
	 */
	public boolean addShip(Ship ship, char col, int row, ShipOrientation orientation) {
		final int originCol = col - 'A';
		final int originRow = row - 1;

		int rangeHorizontal = ship.getLength();
		int rangeVertical = ship.getWidth();

		if (orientation.equals(ShipOrientation.VERTICAL)) {
			rangeHorizontal = ship.getWidth();
			rangeVertical = ship.getLength();
		}
		for (int i = originCol; i < originCol + rangeHorizontal; i++) {
			for (int j = originRow; j < originRow + rangeVertical; j++) {
				final Integer cellIndex = Integer.valueOf(Helper.calculatePairingNumber(i, j));
				final CellTrackingFleet cell = this.getCells().get(cellIndex);
				if (cell == null || (cell.getShip() != null && cell.getShip() != ship)) {
					return false;
				}
			}
		}
		ship.resetPosition();
		for (int i = originCol; i < originCol + rangeHorizontal; i++) {
			for (int j = originRow; j < originRow + rangeVertical; j++) {
				final Integer cellIndex = Integer.valueOf(Helper.calculatePairingNumber(i, j));
				final CellTrackingFleet cell = this.getCells().get(cellIndex);
				cell.setShip(ship);
				cell.setHealth(Game.CELL_UNIT_HEALTH);
				ship.addCell(cell);
			}
		}
		ship.setOrientation(orientation);
		ship.setOrigin(this.getCells().get(Integer.valueOf(Helper.calculatePairingNumber(originCol, originRow))));
		return true;
	}

	// public boolean removeShip(Ship ship) {
	// if (ship.getCaptain().getGridTrackingFleet() == this) {
	// while (true) {
	// if (ship.getCells().size() > 0) {
	// CellTrackingFleet oldCell = ship.getCells().get(0);
	// oldCell.setShip(null);
	// ship.getCells().remove(oldCell);
	// } else {
	// return true;
	// }
	// }
	// } else {
	// return false;
	// }
	// }

	/**
	 * Get the text presentation of the grid. A cell could be presented as:
	 * <ul>
	 * <li>| x | if it DOES NOT have ship on it and it was attacked.</li>
	 * <li>| | if it DOES NOT have ship and has not been yet attacked.</li>
	 * <li>|2-3| if it has ship and it was attacked 3 times and remain 2
	 * health.</li>
	 * <li>| * | if it has ship and it was totally damaged. Health remains
	 * 0.</li>
	 * </ul>
	 * 
	 * A sample of presentation could be:
	 * <p>
	 * |__| A | B | C | D |
	 * </p>
	 * <p>
	 * | 1| _ | _ | _ | _ |
	 * </p>
	 * <p>
	 * | 2| * |1-0|1-0| _ |
	 * </p>
	 * <p>
	 * | 3| x | _ | _ | x |
	 * </p>
	 * <p>
	 * | 4| _ | * |2-1| _ |
	 * </p>
	 */
	@Override
	public String toString() {
		StringBuilder gridPrint = new StringBuilder("|" + String.format(Grid.CELL_SPACE, "") + "|");
		for (int i = 0; i < getWidth(); i++) {
			gridPrint.append(String.format(Grid.CELL_SPACE, (char) ('A' + i)) + "|");
		}
		gridPrint.append("\n");
		for (int rowIndex = 0; rowIndex < getHeight(); rowIndex++) {
			gridPrint.append("|" + String.format(Grid.CELL_SPACE, (rowIndex + 1)) + "|");
			for (int colIndex = 0; colIndex < getWidth(); colIndex++) {
				final int cellIndex = Helper.calculatePairingNumber(colIndex, rowIndex);
				CellTrackingFleet cell = getCells().get(cellIndex);
				if (cell.getShip() != null) {
					if (cell.getHealth() > 0) {
						gridPrint.append(String.format(Grid.CELL_SPACE,
								cell.getHealth() + "-" + cell.getNumberOfTimesBeingAttacked()) + "|");
					} else {
						gridPrint.append(String.format(Grid.CELL_SPACE, "*") + "|");
					}
				} else {
					if (cell.wasAttacked()) {
						gridPrint.append(String.format(Grid.CELL_SPACE, "x") + "|");
					} else {
						gridPrint.append(String.format(Grid.CELL_SPACE, "") + "|");
					}
				}
			}
			gridPrint.append("\n");
		}
		return gridPrint.toString();
	}
}
