package docongbang.com.battleship.api.utils;

public enum AttackStrategy {
	CLASSIC, CROSS, ILLUMINATING_ROCKET
}
