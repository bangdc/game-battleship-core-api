package docongbang.com.battleship.api.utils;

/**
 * The game proposes 3 modes:
 * <ul>
 * <li>Player and Computer</li>
 * <li>Player and Player</li>
 * <li>Computer and Computer - For simulation and fun only.</li>
 * </ul>
 * @author bangdc
 * @since 01-01-2017
 */
public enum GamePlayerMode {
	PLAYER_COMPUTER,
	PLAYER_PLAYER,
	COMPUTER_COMPUTER
}
