package docongbang.com.battleship.api.utils;

/**
 * The game proposes 4 different levels of difficulty. The difficulty is
 * applicable in the mode Player and Computer. It represents how intelligent the
 * computer will be.
 * 
 * @author bangdc
 * @since 01-01-2017
 */
public enum DifficultyLevel {
	NA, HARD, MEDIUM, EASY
}
