package docongbang.com.battleship.api.utils;

/**
 * The orientation of ship on grid
 * 
 * @author bangdc
 * @since 01-01-2017
 */
public enum ShipOrientation {
	VERTICAL, HORIZONTAL
}
