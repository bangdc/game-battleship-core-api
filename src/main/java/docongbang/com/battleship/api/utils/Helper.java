package docongbang.com.battleship.api.utils;

/**
 * This Helper contains useful functions.
 * 
 * @author bangdc
 * @since 29-12-2016
 */
public abstract class Helper {
	/**
	 * Calculate unique number from two numbers. The purpose here is to generate
	 * unique key for a cell based on its column index and row index. Please
	 * check out here for further information about the algorithm:
	 * https://en.wikipedia.org/wiki/Pairing_function
	 * 
	 * @param x
	 *            The first number (Column Index)
	 * @param y
	 *            The second number (Row Index)
	 * @return A unique number
	 */
	public static int calculatePairingNumber(final int x, final int y) {
		return (x + y) * (x + y + 1) / 2 + y;
	}
}
