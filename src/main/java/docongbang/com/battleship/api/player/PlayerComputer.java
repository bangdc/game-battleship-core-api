package docongbang.com.battleship.api.player;

import docongbang.com.battleship.api.game.Game;

/**
 * Player computer
 * 
 * @author bangdc
 * @since 01-01-2017
 * @see Player
 */
public class PlayerComputer extends Player {
	/**
	 * Construct a new computer player
	 * 
	 * @param name
	 * @param gridWidth
	 * @param gridHeight
	 * @param game
	 */
	public PlayerComputer(String name, int gridWidth, int gridHeight, Game game) {
		super(name, gridWidth, gridHeight, game);
	}
}
