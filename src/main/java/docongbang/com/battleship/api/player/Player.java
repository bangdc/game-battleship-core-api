package docongbang.com.battleship.api.player;

import java.util.ArrayList;
import java.util.Random;
import docongbang.com.battleship.api.game.Game;
import docongbang.com.battleship.api.grid.GridTrackingAttack;
import docongbang.com.battleship.api.grid.GridTrackingFleet;
import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.ship.Ship;
import docongbang.com.battleship.api.ship.ShipFactory;
import docongbang.com.battleship.api.ship.civil.SailingShip;
import docongbang.com.battleship.api.ship.naval.NavalShip;
import docongbang.com.battleship.api.ship.naval.SubmarineWithIlluminatingRocket;
import docongbang.com.battleship.api.ship.naval.behaviors.attack.AttackClassic;
import docongbang.com.battleship.api.ship.naval.behaviors.attack.AttackCross;
import docongbang.com.battleship.api.ship.naval.behaviors.attack.AttackWithIlluminatingRocket;
import docongbang.com.battleship.api.ship.naval.exceptions.FactoryCouldNotProduceTheShip;
import docongbang.com.battleship.api.ship.naval.exceptions.OnlySubmarineHaveIlluminatingRocket;
import docongbang.com.battleship.api.utils.AttackStrategy;
import docongbang.com.battleship.api.utils.ShipOrientation;

/**
 * Player of the game.
 * 
 * @author bangdc
 * @since 29-12-2016
 */
public class Player {
	private static final int SUBMARINE_INDEX = 3;

	private String name;

	private GridTrackingFleet gridTrackingFleet;
	private GridTrackingAttack gridTrackingAttackResult;

	private ArrayList<NavalShip> ships = new ArrayList<NavalShip>();
	private SailingShip sailingShip;

	private int numberOfTurnsPlayed = 0;
	private int numberOfFailures = 0;
	private int numberOfConsecutiveFailures = 0;
	private int numberOfTurnsRemaining = 0;

	private Game game;

	/**
	 * Construct a new player
	 * 
	 * @param name
	 *            The name of player
	 * @param gridWidth
	 *            The width of grid.
	 * @param gridHeight
	 *            The height of grid.
	 */
	public Player(final String name, final int gridWidth, final int gridHeight, final Game game) {
		this.name = name;
		this.gridTrackingFleet = new GridTrackingFleet(gridWidth, gridHeight);
		this.gridTrackingAttackResult = new GridTrackingAttack(gridWidth, gridHeight);
		this.game = game;
		this.initShips();
	}

	/**
	 * Player add all ships required by the rules of game. TODO A better way to
	 * organize the ships of player.
	 */
	private void initShips() {
		try {
			ships.add(ShipFactory.createNavalShip("carrier", this));
			ships.add(ShipFactory.createNavalShip("battleship", this));
			ships.add(ShipFactory.createNavalShip("cruiser", this));
			ships.add(ShipFactory.createNavalShip("submarine", this));
			ships.add(ShipFactory.createNavalShip("destroyer", this));
		} catch (OnlySubmarineHaveIlluminatingRocket e) {
			e.printStackTrace();
		} catch (FactoryCouldNotProduceTheShip e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get the submarine of player.
	 * 
	 * @return
	 */
	public NavalShip getSubmarine() {
		return ships.get(SUBMARINE_INDEX);
	}

	/**
	 * This function will place automatically all ships on the grid. It will be
	 * used by computer. Post-conditions: - All ships wasPlacedOnGrid is true
	 * TODO Algorithm to ensure that all ships will always be placed on grid,
	 * even the grid is small. THERE WILL BE A PROBLEM HERE!!!!
	 */
	public void placeShipsAuto() {
		final Random random = new Random();
		GridTrackingFleet grid = this.getGridTrackingFleet();
		for (Ship ship : getShips()) {
			while (true) {
				final char col = (char) (random.nextInt(Game.GRID_WIDTH) + 1 + 'A');
				final int row = random.nextInt(Game.GRID_HEIGHT) + 1;
				final ShipOrientation orientation = (random.nextBoolean()) ? ShipOrientation.HORIZONTAL
						: ShipOrientation.VERTICAL;
				if (grid.addShip(ship, col, row, orientation)) {
					break;
				}
			}
		}
	}

	/**
	 * @return Reference to the grid for tracking ships.
	 */
	public GridTrackingFleet getGridTrackingFleet() {
		return gridTrackingFleet;
	}

	/**
	 * @return Reference to the grid for tracking attack result.
	 */
	public GridTrackingAttack getGridTrackingAttackResult() {
		return gridTrackingAttackResult;
	}

	/**
	 * @return The opponent.
	 */
	public Player getOpponent() {
		if (game.getPlayer1() == this) {
			return game.getPlayer2();
		}
		if (game.getPlayer2() == this) {
			return game.getPlayer1();
		}
		return null;
	}

	/**
	 * @return Number of consecutive attack failures.
	 */
	public int getNumberOfConsecutiveFailures() {
		return numberOfConsecutiveFailures;
	}

	/**
	 * @return Total number of failures.
	 */
	public int getNumberOfFailures() {
		return numberOfFailures;
	}

	/**
	 * @return The name of the player
	 */
	public String getName() {
		return name;
	}

	// /**
	// * @return A text description of status of all ships.
	// */
	// public String getAllAvailableShipsAsString() {
	// StringBuilder stringShips = new StringBuilder();
	// for (int i = 0; i < ships.size(); i++) {
	// stringShips.append(String.format("%5s", "#" + i + ": "));
	// stringShips.append(String.format("%10s", ships.get(i).getName() + "; "));
	// if (ships.get(i).isAlive()) {
	// stringShips.append(
	// String.format("%10s", ships.get(i).getCapabilityOfKilling() + "
	// capability of killing; "));
	// } else {
	// stringShips.append(String.format("%10s", "TOTALLY DESTROYED!"));
	// }
	// stringShips.append("\n");
	// }
	// return stringShips.toString();
	// }

	// /**
	// * @return A text description of position of all ships.
	// */
	// public String getShipsPositionAsString() {
	// StringBuilder stringShips = new StringBuilder();
	// for (int i = 0; i < ships.size(); i++) {
	// stringShips.append(String.format("%5s", "#" + i + ": "));
	// stringShips.append(String.format("%10s", ships.get(i).getName() + "; "));
	// if (ships.get(i).wasPlacedOnGrid()) {
	// stringShips.append(String.format("%10s", "At " +
	// ships.get(i).getCells().get(0).toString()));
	// } else {
	// stringShips.append(String.format("%10s", "NOT YET PLACED!"));
	// }
	// stringShips.append("\n");
	// }
	// return stringShips.toString();
	// }

	/**
	 * @return Check whether if all ships were destroyed.
	 */
	public boolean lostAllShips() {
		boolean lostAll = true;
		for (int i = 0; i < ships.size(); i++) {
			if (ships.get(i).isAlive()) {
				lostAll = false;
				break;
			}
		}
		return lostAll;
	}

	/**
	 * @return All ships.
	 */
	public ArrayList<NavalShip> getShips() {
		return ships;
	}

	/**
	 * If player failed an attack
	 */
	public void fail() {
		setNumberOfFailures(getNumberOfFailures() + 1);
		setNumberOfConsecutiveFailures(getNumberOfConsecutiveFailures() + 1);
	}

	/**
	 * Attack the opponent. A success attack will cause damage for at least one
	 * ship of the opponent.
	 * 
	 * @param dutyShip
	 * @param target
	 * @param attackStrategy
	 * @return true if success
	 */
	public boolean attack(final NavalShip dutyShip, final CellTrackingFleet target,
			final AttackStrategy attackStrategy) {
		final Player opponent = getOpponent();
		ArrayList<Integer> shipsHealthBeforeAttacking = new ArrayList<Integer>();
		for (NavalShip ship : opponent.getShips()) {
			shipsHealthBeforeAttacking.add(ship.getHealth());
		}

		switch (attackStrategy) {
		case CLASSIC:
			dutyShip.setAttackStrategy(new AttackClassic(dutyShip));
			break;
		case CROSS:
			dutyShip.setAttackStrategy(new AttackCross(dutyShip));
			break;
		case ILLUMINATING_ROCKET:
			dutyShip.setAttackStrategy(new AttackWithIlluminatingRocket((SubmarineWithIlluminatingRocket) dutyShip));
			break;
		default:
			break;
		}

		dutyShip.attack(target, dutyShip.getCapabilityOfKilling());

		boolean successAttack = false;
		for (int i = 0; i < opponent.getShips().size(); i++) {
			if (opponent.getShips().get(i).getHealth() < shipsHealthBeforeAttacking.get(i).intValue()) {
				successAttack = true;
				break;
			}
		}
		return successAttack;
	}

	/**
	 * @return number of turns that player can use right after finishing the
	 *         current turn.
	 */
	public int getNumberOfTurnsRemaining() {
		return numberOfTurnsRemaining;
	}

	/**
	 * Set the number of turns remaining
	 * 
	 * @param numberOfTurnsRemaining
	 */
	public void setNumberOfTurnsRemaining(final int numberOfTurnsRemaining) {
		this.numberOfTurnsRemaining = numberOfTurnsRemaining;
	}

	/**
	 * @return The game that player is currently on.
	 */
	public Game getGame() {
		return game;
	}

	/**
	 * Get the index of submarine
	 * 
	 * @return
	 */
	public static int getSubmarineIndex() {
		return SUBMARINE_INDEX;
	}

	/**
	 * @return true if all ships of user were placed on the grid.
	 */
	public boolean allShipsPlacedOnGrid() {
		if (ships.size() > 0) {
			for (Ship ship : ships) {
				if (ship.wasPlacedOnGrid() == false) {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Set the number of turns the player have played so far.
	 * 
	 * @param numberOfTurnsPlayed
	 */
	public void setNumberOfTurnsPlayed(int numberOfTurnsPlayed) {
		this.numberOfTurnsPlayed = numberOfTurnsPlayed;
	}

	/**
	 * Get the number of turns the player have played so far.
	 * 
	 * @return
	 */
	public int getNumberOfTurnsPlayed() {
		return numberOfTurnsPlayed;
	}

	/**
	 * Check if the player is able to use illuminating rocket with submarine
	 * 
	 * @return
	 */
	public boolean ableToUseRocket() {
		NavalShip submarine = getSubmarine();
		if (submarine instanceof SubmarineWithIlluminatingRocket) {
			SubmarineWithIlluminatingRocket submarineWithRocket = (SubmarineWithIlluminatingRocket) submarine;
			if (submarineWithRocket.getNumberOfRockets() > 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Unlock the illuminating rocket with submarine
	 */
	public void unlockSubmarineWithRocket() {
		NavalShip submarine = this.getSubmarine();
		if (submarine.isAlive()) {
			if (submarine instanceof SubmarineWithIlluminatingRocket) {
				SubmarineWithIlluminatingRocket submarineRocket = (SubmarineWithIlluminatingRocket) submarine;
				submarineRocket.setNumberOfRockets(submarineRocket.getNumberOfRockets() + 1);
			} else {
				try {
					ships.set(Player.getSubmarineIndex(), new SubmarineWithIlluminatingRocket(submarine));
				} catch (OnlySubmarineHaveIlluminatingRocket e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Get the most powerfull ship in fleet
	 * 
	 * @return
	 */
	public NavalShip getMostPowerfulShip() {
		int maxCapabilityOfKilling = 0;
		NavalShip mostPowerfulShip = ships.get(0);
		for (NavalShip ship : ships) {
			if (ship.isAlive()) {
				if (ship.getCapabilityOfKilling() > maxCapabilityOfKilling) {
					mostPowerfulShip = ship;
					maxCapabilityOfKilling = ship.getCapabilityOfKilling();
				}
			}
		}
		return mostPowerfulShip;
	}

	/**
	 * Upgrade a ship at specific cell
	 * 
	 * @param ship
	 * @param cell
	 */
	public void upgradeShip(NavalShip ship, CellTrackingFleet cell) {
		if (ship.getCaptain() == this) {
			ship.upgrade(cell);
		}
	}

	/**
	 * Upgrade randomly a ship and a cell
	 */
	public void upgradeShipRandomly() {
		NavalShip shipToUpgrade = getShips().get(new Random().nextInt(getShips().size() - 1));
		CellTrackingFleet cellToUpgrade = shipToUpgrade.getCells()
				.get(new Random().nextInt(shipToUpgrade.getCells().size() - 1));
		shipToUpgrade.upgrade(cellToUpgrade);
	}

	/**
	 * Get target randomly
	 * 
	 * @return
	 */
	public CellTrackingFleet getTargetRandomly() {
		ArrayList<Integer> availableCellsIndex = getGridTrackingAttackResult().getCellsIndex();
		final Random random = new Random();
		final int cellIndex = availableCellsIndex.get(random.nextInt(availableCellsIndex.size()));
		return getOpponent().getGridTrackingFleet().getCells().get(cellIndex);
	}

	/**
	 * Get attack strategy randomly
	 * 
	 * @return
	 */
	public AttackStrategy getAttackStrategyRandomly() {
		if (new Random().nextBoolean()) {
			return AttackStrategy.CLASSIC;
		} else {
			return AttackStrategy.CROSS;
		}
	}

	/**
	 * Set the number of consecutive failures
	 * 
	 * @param numberOfConsecutiveFailures
	 */
	public void setNumberOfConsecutiveFailures(int numberOfConsecutiveFailures) {
		this.numberOfConsecutiveFailures = numberOfConsecutiveFailures;
	}

	/**
	 * Set the number of failures
	 * 
	 * @param numberOfFailures
	 */
	public void setNumberOfFailures(int numberOfFailures) {
		this.numberOfFailures = numberOfFailures;
	}

	/**
	 * Get the sailing ship
	 * 
	 * @return
	 */
	public SailingShip getSailingShip() {
		return sailingShip;
	}

	/**
	 * Set the sailing ship
	 * 
	 * @param sailingShip
	 */
	public void setSailingShip(SailingShip sailingShip) {
		this.sailingShip = sailingShip;
	}
}