package docongbang.com.battleship.api.ship;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.player.Player;

public class BasicShipTest {
	/**
	 * Make sure assertions are enabled with VM argument: -ea
	 */
	@Test(expected = AssertionError.class)
	public void testAssertionsEnabled() {
		assert false;
	}

	@Test
	public void testShip() {
		Player captain = mock(Player.class);
		Ship ship = new BasicShip("Titanic", 2, 1, captain);
		assertEquals(2, ship.getWidth());
		assertEquals(1, ship.getLength());
		assertEquals("Titanic", ship.getName());
		assertEquals(captain, ship.getCaptain());
		assertEquals(0, ship.getHealth());
		assertEquals(0, ship.getCells().size());
		assertFalse(ship.wasPlacedOnGrid());
		assertFalse(ship.isAlive());

		CellTrackingFleet cell1 = mock(CellTrackingFleet.class);
		CellTrackingFleet cell2 = mock(CellTrackingFleet.class);

		ship.addCell(cell1);
		ship.addCell(cell2);

		when(cell1.getShip()).thenReturn(ship);
		when(cell2.getShip()).thenReturn(ship);
		when(cell1.getHealth()).thenReturn(2);
		when(cell2.getHealth()).thenReturn(2);

		assertEquals(2, ship.getCells().size());
		assertTrue(ship.wasPlacedOnGrid());
		assertEquals(4, ship.getHealth());

	}
}
