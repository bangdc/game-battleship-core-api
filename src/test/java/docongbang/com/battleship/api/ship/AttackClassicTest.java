package docongbang.com.battleship.api.ship;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import docongbang.com.battleship.api.grid.GridTrackingFleet;
import docongbang.com.battleship.api.grid.cell.CellBomb;
import docongbang.com.battleship.api.grid.cell.CellTrackingFleet;
import docongbang.com.battleship.api.player.Player;
import docongbang.com.battleship.api.ship.civil.CivilShip;
import docongbang.com.battleship.api.ship.civil.SailingShip;
import docongbang.com.battleship.api.ship.naval.BasicNavalShip;
import docongbang.com.battleship.api.ship.naval.NavalShip;
import docongbang.com.battleship.api.ship.naval.behaviors.attack.AttackClassic;
import docongbang.com.battleship.api.utils.Helper;
import docongbang.com.battleship.api.utils.ShipOrientation;

public class AttackClassicTest {
	private GridTrackingFleet grid;
	private Player captain;
	private Player opponent;
	private NavalShip attackingShip;
	private NavalShip attackedShip1_1;
	private NavalShip attackedShip1_4;
	private CivilShip attackedCivilShip;
	private CellBomb cellBomb;
	private int captainTurns = 1;
	private int opponentTurns = 0;

	/**
	 * Make sure assertions are enabled with VM argument: -ea
	 */
	@Test(expected = AssertionError.class)
	public void testAssertionsEnabled() {
		assert false;
	}

	@Before
	public void init() {
		grid = new GridTrackingFleet(7, 7);

		captain = mock(Player.class);
		opponent = mock(Player.class);
		cellBomb = mock(CellBomb.class);

		when(captain.getName()).thenReturn("Captain");
		when(opponent.getName()).thenReturn("Opponent");
		when(captain.getOpponent()).thenReturn(opponent);
		when(captain.getNumberOfTurnsRemaining()).thenReturn(captainTurns);
		when(opponent.getNumberOfTurnsRemaining()).thenReturn(opponentTurns);
		when(opponent.getGridTrackingFleet()).thenReturn(grid);

		attackedShip1_1 = new BasicNavalShip("1", 1, 1, opponent);
		attackedShip1_4 = new BasicNavalShip("2", 1, 4, opponent);

		attackedCivilShip = SailingShip.getSailingShipOfPlayer1(opponent);

		attackingShip = new BasicNavalShip("Duty Ship", 1, 1, captain);
		attackingShip.setAttackStrategy(new AttackClassic(attackingShip));
	}

	// Test cases:
	// Cell: empty || has naval ship || has civil ship || has bomb.
	// Bomb: already triggered || not yet triggered.
	// Bomb position: corner of grid || border of grid || somewhere else.
	// Civil ship: already destroyed || not yet destroyed
	// Ship position: at bomb position || at round 1 of bomb || at round 2 of
	// bomb || out of range of bomb.
	@Test
	public void testAttackCell() {
		CellTrackingFleet target = grid.getCells().get(0);
		// target.increaseHealth(100);
		attackingShip.attack(target, 1);
		assertEquals(99, target.getHealth());
	}

	@Test
	public void testAttackOnNavalShip() {
		grid.addShip(attackedShip1_1, 'A', 1, ShipOrientation.HORIZONTAL);
		int beforeAttackHealth = attackedShip1_1.getHealth();

		CellTrackingFleet targetFail = grid.getCells().get(Helper.calculatePairingNumber(1, 1));
		CellTrackingFleet targetSuccess = grid.getCells().get(Helper.calculatePairingNumber(0, 0));

		// Attack on the wrong target
		attackingShip.attack(targetFail, 2);
		assertEquals(beforeAttackHealth, attackedShip1_1.getHealth());

		// Attack on the right target but with no damage
		attackingShip.attack(targetSuccess, 0);
		assertEquals(beforeAttackHealth, attackedShip1_1.getHealth());

		// Attack on the right target but with damage
		attackingShip.attack(targetSuccess, 100);
		assertEquals(0, attackedShip1_1.getHealth());
	}

	@Test
	public void testAttackOnBombAtCorner() {
		grid.getCells().put(Helper.calculatePairingNumber(0, 0), CellBomb.getCellOnPlayer1Grid(0, 0));
		CellBomb target = (CellBomb) grid.getCells().get(Helper.calculatePairingNumber(0, 0));
		CellTrackingFleet cellOnRound1 = grid.getCells().get(Helper.calculatePairingNumber(1, 0));
		CellTrackingFleet cellOnRound2 = grid.getCells().get(Helper.calculatePairingNumber(2, 2));
		CellTrackingFleet cellOnRound3 = grid.getCells().get(Helper.calculatePairingNumber(2, 4));

		int beforeAttackHealth = 5;
		// cellOnRound1.increaseHealth(beforeAttackHealth);
		// cellOnRound2.increaseHealth(beforeAttackHealth);
		// cellOnRound3.increaseHealth(beforeAttackHealth);
		// target.increaseHealth(beforeAttackHealth);
		int damage = 1;
		attackingShip.attack(target, damage);

		// assertEquals(beforeAttackHealth - damage - CellBomb.BOMB_DAMAGE,
		// target.getHealth());
		// assertEquals(beforeAttackHealth - CellBomb.BOMB_DAMAGE_SURROUND_1,
		// cellOnRound1.getHealth());
		// assertEquals(beforeAttackHealth - CellBomb.BOMB_DAMAGE_SURROUND_2,
		// cellOnRound2.getHealth());
		assertEquals(beforeAttackHealth, cellOnRound3.getHealth());

	}

	@Test
	public void testAttackOnBombAtBorder() {
		when(cellBomb.doesHaveBomb()).thenReturn(true);
		when(cellBomb.getColIndex()).thenReturn(3);
		when(cellBomb.getRowIndex()).thenReturn(6);
		when(cellBomb.wasTriggered()).thenReturn(false);

		grid.getCells().put(Helper.calculatePairingNumber(3, 6), cellBomb);
		CellBomb target = (CellBomb) grid.getCells().get(Helper.calculatePairingNumber(3, 6));
		CellTrackingFleet cellOnRound1 = grid.getCells().get(Helper.calculatePairingNumber(2, 5));
		CellTrackingFleet cellOnRound2 = grid.getCells().get(Helper.calculatePairingNumber(1, 4));
		CellTrackingFleet cellOnRound3 = grid.getCells().get(Helper.calculatePairingNumber(3, 3));

		int beforeAttackHealth = 5;
		// cellOnRound1.increaseHealth(beforeAttackHealth);
		// cellOnRound2.increaseHealth(beforeAttackHealth);
		// cellOnRound3.increaseHealth(beforeAttackHealth);
		// target.increaseHealth(beforeAttackHealth);
		int damage = 1;
		attackingShip.attack(target, damage);

		// assertEquals(beforeAttackHealth - CellBomb.BOMB_DAMAGE_SURROUND_1,
		// cellOnRound1.getHealth());
		// assertEquals(beforeAttackHealth - CellBomb.BOMB_DAMAGE_SURROUND_2,
		// cellOnRound2.getHealth());
		assertEquals(beforeAttackHealth, cellOnRound3.getHealth());

	}

	@Test
	public void testAttackOnBombAlreadyTriggered() {
		when(cellBomb.doesHaveBomb()).thenReturn(true);
		when(cellBomb.getColIndex()).thenReturn(3);
		when(cellBomb.getRowIndex()).thenReturn(6);
		when(cellBomb.wasTriggered()).thenReturn(true);

		grid.getCells().put(Helper.calculatePairingNumber(3, 6), cellBomb);
		CellBomb target = (CellBomb) grid.getCells().get(Helper.calculatePairingNumber(3, 6));
		CellTrackingFleet cellOnRound1 = grid.getCells().get(Helper.calculatePairingNumber(2, 5));
		CellTrackingFleet cellOnRound2 = grid.getCells().get(Helper.calculatePairingNumber(1, 4));
		CellTrackingFleet cellOnRound3 = grid.getCells().get(Helper.calculatePairingNumber(3, 3));

		int beforeAttackHealth = 5;
		// cellOnRound1.increaseHealth(beforeAttackHealth);
		// cellOnRound2.increaseHealth(beforeAttackHealth);
		// cellOnRound3.increaseHealth(beforeAttackHealth);
		// target.increaseHealth(beforeAttackHealth);
		int damage = 1;
		attackingShip.attack(target, damage);

		assertEquals(beforeAttackHealth, cellOnRound1.getHealth());
		assertEquals(beforeAttackHealth, cellOnRound2.getHealth());
		assertEquals(beforeAttackHealth, cellOnRound3.getHealth());

		// verify(opponent, times(1)).setNumberOfTurnsRemaining(opponentTurns +
		// AttackClassic.NUMBER_OF_TURNS_COST);
		// verify(captain, times(1)).setNumberOfTurnsRemaining(captainTurns -
		// AttackClassic.NUMBER_OF_TURNS_COST);
	}

	@Test
	public void testAttackOnCivilShip() {
		grid.addShip(attackedCivilShip, 'A', 1, ShipOrientation.HORIZONTAL);
		// grid.getCells().get(Helper.calculatePairingNumber(0,
		// 0)).increaseHealth(3);
		int beforeAttackHealth = attackedCivilShip.getHealth();

		CellTrackingFleet targetFail = grid.getCells().get(Helper.calculatePairingNumber(1, 1));
		CellTrackingFleet targetSuccess = grid.getCells().get(Helper.calculatePairingNumber(0, 0));

		InOrder order = inOrder(opponent, captain);

		// Attack on the wrong target
		attackingShip.attack(targetFail, 2);
		assertEquals(beforeAttackHealth, attackedCivilShip.getHealth());
		// order.verify(captain).setNumberOfTurnsRemaining(captainTurns -
		// AttackClassic.NUMBER_OF_TURNS_COST);
		// order.verify(opponent).setNumberOfTurnsRemaining(opponentTurns +
		// AttackClassic.NUMBER_OF_TURNS_COST);

		// Attack on the right target but with no damage
		attackingShip.attack(targetSuccess, 0);
		assertEquals(beforeAttackHealth, attackedCivilShip.getHealth());
		// order.verify(captain).setNumberOfTurnsRemaining(captainTurns -
		// AttackClassic.NUMBER_OF_TURNS_COST);
		// order.verify(opponent).setNumberOfTurnsRemaining(opponentTurns +
		// AttackClassic.NUMBER_OF_TURNS_COST);

		// Attack on the right target but with little damage
		attackingShip.attack(targetSuccess, 1);
		assertEquals(beforeAttackHealth - 1, attackedCivilShip.getHealth());
		// order.verify(captain).setNumberOfTurnsRemaining(captainTurns -
		// AttackClassic.NUMBER_OF_TURNS_COST);
		// order.verify(opponent).setNumberOfTurnsRemaining(opponentTurns +
		// AttackClassic.NUMBER_OF_TURNS_COST);

		// Attack on the right target but with total damage
		attackingShip.attack(targetSuccess, 100);
		assertEquals(0, attackedCivilShip.getHealth());

	}

	@Test
	public void testAttackOnNavalShipAndBomb() {
		// Round 1
		grid.addShip(attackedShip1_1, 'C', 2, ShipOrientation.HORIZONTAL);
		// Round 2
		grid.addShip(attackedShip1_4, 'B', 5, ShipOrientation.HORIZONTAL);
		// Out of range
		grid.addShip(attackedCivilShip, 'A', 1, ShipOrientation.HORIZONTAL);
		int beforeAttackHealth = attackedCivilShip.getHealth();

		assertTrue(attackedShip1_1.isAlive());
		assertTrue(attackedShip1_4.isAlive());

		when(cellBomb.doesHaveBomb()).thenReturn(true);
		when(cellBomb.getColIndex()).thenReturn(3); // D
		when(cellBomb.getRowIndex()).thenReturn(3); // 4
		when(cellBomb.wasTriggered()).thenReturn(false);

		grid.getCells().put(Helper.calculatePairingNumber(3, 3), cellBomb);
		CellBomb target = (CellBomb) grid.getCells().get(Helper.calculatePairingNumber(3, 3));

		int damage = 1;
		attackingShip.attack(target, damage);

		assertFalse(attackedShip1_1.isAlive());
		assertFalse(attackedShip1_4.isAlive());
		assertEquals(beforeAttackHealth, attackedCivilShip.getHealth());
	}

	@Test
	public void testAttackOnCivilShipAndBomb() {
		// Round 2
		grid.addShip(attackedCivilShip, 'B', 2, ShipOrientation.HORIZONTAL);
		int beforeAttackHealth = attackedCivilShip.getHealth();

		when(cellBomb.doesHaveBomb()).thenReturn(true);
		when(cellBomb.getColIndex()).thenReturn(3); // D
		when(cellBomb.getRowIndex()).thenReturn(3); // 4
		when(cellBomb.wasTriggered()).thenReturn(false);

		grid.getCells().put(Helper.calculatePairingNumber(3, 3), cellBomb);
		CellBomb target = (CellBomb) grid.getCells().get(Helper.calculatePairingNumber(3, 3));

		int damage = 1;
		attackingShip.attack(target, damage);

		// assertEquals(beforeAttackHealth - CellBomb.BOMB_DAMAGE_SURROUND_2,
		// attackedCivilShip.getHealth());
	}
}
