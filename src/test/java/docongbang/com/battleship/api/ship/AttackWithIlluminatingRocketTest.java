package docongbang.com.battleship.api.ship;

import org.junit.Test;

public class AttackWithIlluminatingRocketTest {
	/**
	 * Make sure assertions are enabled with VM argument: -ea
	 */
	@Test(expected=AssertionError.class)
    public void testAssertionsEnabled() {
        assert false;
    }
	
	/**
	 * Test cases:
	 *  - Ship is submarine equipped rocket weapon
	 *  - Ship is not submarine equipped rocket weapon
	 *  - Ship does not have enough rocket weapon = run out of rocket.
	 */
}
