package docongbang.com.battleship.api.utils;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.Random;
/**
 * Test suite for Helper class
 * @author bangdc
 * @since 02-01-2017
 */
public class HelperTest {
	/**
	 * Make sure assertions are enabled with VM argument: -ea
	 */
	@Test(expected=AssertionError.class)
    public void testAssertionsEnabled() {
        assert false;
    }
	/**
	 * Test pairing function.
	 * Make sure two numbers generate an unique key.
	 */
	@Test
	public void testCalculatePairingNumber() {
		final Random random = new Random();
		final int x = random.nextInt();
		final int y = random.nextInt();
		assertNotEquals(Helper.calculatePairingNumber(x, y), Helper.calculatePairingNumber(x + 1, y));
		assertNotEquals(Helper.calculatePairingNumber(x, y), Helper.calculatePairingNumber(x, y + 1));
		assertNotEquals(Helper.calculatePairingNumber(x, y + 1), Helper.calculatePairingNumber(x + 1, y));
		if (x != y) {
			assertNotEquals(Helper.calculatePairingNumber(x, y), Helper.calculatePairingNumber(y, x));
		}
	}
}
