package docongbang.com.battleship.api;

import org.junit.Test;

import docongbang.com.battleship.api.game.Game;
import docongbang.com.battleship.api.utils.DifficultyLevel;
import docongbang.com.battleship.api.utils.GamePlayerMode;

import static org.junit.Assert.*;

public class BattleshipTest {
	/**
	 * Make sure assertions are enabled with VM argument: -ea
	 */
	@Test(expected = AssertionError.class)
	public void testAssertionsEnabled() {
		assert false;
	}

	@Test
	public void testBattleship() {
		Game gameMode2Players = new Game("A", "B");
		Game gameMode1Player = new Game("C", DifficultyLevel.EASY);
		Game gameMode0Player = new Game();

		assertEquals(GamePlayerMode.COMPUTER_COMPUTER, gameMode0Player.getMode());
		assertEquals(GamePlayerMode.PLAYER_COMPUTER, gameMode1Player.getMode());
		assertEquals(GamePlayerMode.PLAYER_PLAYER, gameMode2Players.getMode());

		assertEquals("A", gameMode2Players.getPlayer1().getName());
		assertEquals("B", gameMode2Players.getPlayer2().getName());
		assertEquals("C", gameMode1Player.getPlayer1().getName());

		assertEquals(DifficultyLevel.NA, gameMode2Players.getDifficulty());

		gameMode1Player.setDifficulty(DifficultyLevel.HARD);
		assertEquals(DifficultyLevel.HARD, gameMode1Player.getDifficulty());

		gameMode1Player.setDifficulty(DifficultyLevel.MEDIUM);
		assertEquals(DifficultyLevel.MEDIUM, gameMode1Player.getDifficulty());

		gameMode1Player.setDifficulty(DifficultyLevel.EASY);
		assertEquals(DifficultyLevel.EASY, gameMode1Player.getDifficulty());
	}

	@Test(expected = AssertionError.class)
	public void testBattleshipWinnerLoser() {
		Game game1 = new Game("A", "B");
		Game game2 = new Game("C", DifficultyLevel.EASY);
		// Loser is not player of the game, but of other game
		game1.setLoser(game2.getPlayer1());
	}

	@Test(expected = AssertionError.class)
	public void testBattleshipWinnerLoser1() {
		Game game1 = new Game("A", "B");
		Game game2 = new Game("C", DifficultyLevel.EASY);
		// Winner is not player of the game, but of other game
		game2.setWinner(game1.getPlayer2());
	}

	@Test(expected = AssertionError.class)
	public void testBattleshipWinnerLoser2() {
		Game game1 = new Game("A", "B");
		// Winner cannot be loser.
		game1.setWinner(game1.getPlayer2());
		game1.setLoser(game1.getWinner());
	}

	@Test(expected = AssertionError.class)
	public void testBattleshipWinnerLoser3() {
		Game game1 = new Game("A", "B");
		Game game2 = new Game("C", DifficultyLevel.EASY);
		// Winner is not player of the game, but of other game
		game2.setWinner(game1.getPlayer2());
	}

	@Test
	public void testBattleshipWinnerLoser4() {
		Game game1 = new Game("A", "B");
		game1.setLoser(game1.getPlayer1());
		game1.setWinner(game1.getPlayer2());
		assertEquals(game1.getPlayer1(), game1.getLoser());
		assertEquals(game1.getPlayer2(), game1.getWinner());
	}

	@Test(expected = AssertionError.class)
	public void testBattleshipCurrentPlayer() {
		Game game1 = new Game("A", "B");
		Game game2 = new Game("C", DifficultyLevel.EASY);
		// Current player is not player of the game, but of other game
		game2.setCurrentPlayer(game1.getPlayer2());
	}

	@Test
	public void testBattleshipCurrentPlayer1() {
		Game game1 = new Game("A", "B");

		game1.setCurrentPlayer(game1.getPlayer2());
		assertEquals(game1.getPlayer2(), game1.getCurrentPlayer());

		game1.setCurrentPlayer(game1.getPlayer1());
		assertEquals(game1.getPlayer1(), game1.getCurrentPlayer());
	}

	@Test
	public void testBattleshipRun() {
		Game gameMode0Player = new Game();
		gameMode0Player.initBombAndSailingShip();

		// gameMode0Player.start();
		assertTrue(gameMode0Player.getLoser().lostAllShips());
		assertFalse(gameMode0Player.getWinner().lostAllShips());
	}
}
