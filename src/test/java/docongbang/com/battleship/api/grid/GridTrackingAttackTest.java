package docongbang.com.battleship.api.grid;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Hashtable;

import org.junit.Test;

import docongbang.com.battleship.api.grid.cell.CellTrackingAttack;

public class GridTrackingAttackTest {
	/**
	 * Make sure assertions are enabled with VM argument: -ea
	 */
	@Test(expected=AssertionError.class)
    public void testAssertionsEnabled() {
        assert false;
    }
	
	@Test(expected = AssertionError.class)
	public void testGrid() {
		new GridTrackingAttack(0, 0);
		new GridTrackingAttack(0, 1);
		new GridTrackingAttack(1, 0);
	}
	
	@Test
	public void testGrid2() {
		GridTrackingAttack grid = new GridTrackingAttack(1, 5);
		assertEquals(1, grid.getWidth());
		assertEquals(5, grid.getHeight());
		ArrayList<Integer> cellsIndex = grid.getCellsIndex();
		Hashtable<Integer, CellTrackingAttack> cells = grid.getCells();
		assertEquals(cells.size(), cellsIndex.size());
		for (int i = 0; i < cellsIndex.size(); i++) {
			assertNotNull(cells.get(cellsIndex.get(i)));
		}
	}
}
