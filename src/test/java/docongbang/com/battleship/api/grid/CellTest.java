package docongbang.com.battleship.api.grid;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import static org.mockito.Mockito.*;

import docongbang.com.battleship.api.grid.cell.Cell;
import docongbang.com.battleship.api.ship.Ship;

/**
 * Test suite for Cell
 * 
 * @author bangdc
 * @since 02-01-2017
 * @see Cell
 */
public class CellTest {
	private Cell originCell;
	private Cell leftCell;
	private Cell topCell;
	private Cell normalCell;

	/**
	 * Make sure assertions are enabled with VM argument: -ea
	 */
	@Test(expected = AssertionError.class)
	public void testAssertionsEnabled() {
		assert false;
	}

	/**
	 * 
	 */
	@Before
	public void init() {
		originCell = new Cell(0, 0);
		leftCell = new Cell(0, 1);
		topCell = new Cell(1, 0);
		normalCell = new Cell(7, 9);
	}

	/**
	 * 
	 */
	@Test(expected = AssertionError.class)
	public void testInvalidCell() {
		new Cell(-1, -1);
	}

	// /**
	// *
	// */
	// @Test
	// public void testCell() {
	// assertEquals(0, originCell.getColIndex());
	// assertEquals(0, originCell.getRowIndex());
	// assertEquals(7, normalCell.getColIndex());
	// assertEquals(9, normalCell.getRowIndex());
	//
	// assertEquals(0, normalCell.getHealth());
	// assertEquals(0, originCell.getNumberOfTimesBeingAttacked());
	//
	// assertNull(leftCell.getShip());
	//
	// assertFalse(leftCell.doesHaveShip());
	// assertFalse(topCell.wasAttacked());
	// }
	// /**
	// *
	// */
	// @Test
	// public void testDecreaseHealth() {
	// Ship ship = mock(Ship.class);
	// normalCell.setShip(ship);
	// normalCell.decreaseHealth(200);
	// assertEquals(0, normalCell.getHealth());
	// normalCell.increaseHealth(200);
	// normalCell.decreaseHealth(100);
	// assertEquals(100, normalCell.getHealth());
	// }
	// /**
	// *
	// */
	// @Test
	// public void testIncreaseHealth() {
	// Ship ship = mock(Ship.class);
	// leftCell.setShip(ship);
	// leftCell.increaseHealth(200);
	// assertEquals(200, leftCell.getHealth());
	// }
	/**
	 * Test toString() method
	 */
	@Test
	public void testToString() {
		assertEquals("(A,1)", originCell.toString());
		assertEquals("(B,1)", topCell.toString());
		assertEquals("(A,2)", leftCell.toString());
		assertEquals("(H,10)", normalCell.toString());
	}
}
