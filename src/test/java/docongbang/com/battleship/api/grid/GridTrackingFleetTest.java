package docongbang.com.battleship.api.grid;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import docongbang.com.battleship.api.player.Player;
import docongbang.com.battleship.api.ship.BasicShip;
import docongbang.com.battleship.api.ship.Ship;
import docongbang.com.battleship.api.utils.ShipOrientation;

public class GridTrackingFleetTest {
	/**
	 * Make sure assertions are enabled with VM argument: -ea
	 */
	@Test(expected = AssertionError.class)
	public void testAssertionsEnabled() {
		assert false;
	}

	@Test
	public void testAddShip() {
		GridTrackingFleet gridOneCell = new GridTrackingFleet(1, 1); // 1 col; 1
																		// row
		GridTrackingFleet gridOneCol = new GridTrackingFleet(1, 7); // 1 col; 7
																	// row
		GridTrackingFleet gridOneRow = new GridTrackingFleet(5, 1); // 5 col; 1
																	// row
		GridTrackingFleet grid = new GridTrackingFleet(5, 7); // 5 col; 7 row

		Player captain = mock(Player.class);

		Ship ship0 = new BasicShip("Biggest", 99, 99, captain);
		Ship ship1 = new BasicShip("1", 1, 1, captain);
		Ship ship2 = new BasicShip("2", 1, 5, captain); // width 1, length 5
		Ship ship3 = new BasicShip("3", 5, 1, captain); // width 5, length 1
		Ship ship4 = new BasicShip("4", 5, 7, captain); // width 5, length 7
		Ship ship5 = new BasicShip("5", 7, 5, captain); // width 7, length 5
		Ship ship6 = new BasicShip("6", 3, 3, captain); // width 3, length 3

		// Test with a biggest ship, that will never fit the grid.
		when(captain.getGridTrackingFleet()).thenReturn(gridOneCell);
		assertFalse(gridOneCell.addShip(ship0, 'A', 1, ShipOrientation.HORIZONTAL));
		when(captain.getGridTrackingFleet()).thenReturn(gridOneCol);
		assertFalse(gridOneCol.addShip(ship0, 'A', 1, ShipOrientation.VERTICAL));
		when(captain.getGridTrackingFleet()).thenReturn(gridOneRow);
		assertFalse(gridOneRow.addShip(ship0, 'A', 1, ShipOrientation.HORIZONTAL));
		when(captain.getGridTrackingFleet()).thenReturn(grid);
		assertFalse(grid.addShip(ship0, 'A', 1, ShipOrientation.VERTICAL));

		// Test with the smallest ship of size 1-1
		when(captain.getGridTrackingFleet()).thenReturn(grid);
		assertFalse(grid.addShip(ship1, 'a', 1, ShipOrientation.HORIZONTAL));
		assertFalse(grid.addShip(ship1, 'A', 0, ShipOrientation.VERTICAL));
		assertFalse(grid.addShip(ship1, 'A', -1, ShipOrientation.HORIZONTAL));
		when(captain.getGridTrackingFleet()).thenReturn(gridOneCell);
		assertTrue(gridOneCell.addShip(ship1, 'A', 1, ShipOrientation.HORIZONTAL));
		assertTrue(gridOneCell.addShip(ship1, 'A', 1, ShipOrientation.VERTICAL));

		// Test with grid having only one column
		when(captain.getGridTrackingFleet()).thenReturn(gridOneCol);
		assertFalse(gridOneCol.addShip(ship2, 'A', 1, ShipOrientation.HORIZONTAL));
		assertTrue(gridOneCol.addShip(ship2, 'A', 1, ShipOrientation.VERTICAL));

		// //Test with grid having only one row
		// when(captain.getGridTrackingFleet()).thenReturn(gridOneRow);
		// assertTrue(gridOneRow.addShip(ship3, 'A', 1,
		// ShipOrientation.VERTICAL));
		// //TODO check if not removing ship here
		// gridOneRow.removeShip(ship3);
		// assertFalse(gridOneRow.addShip(ship3, 'A', 1,
		// ShipOrientation.HORIZONTAL));
		//
		// //Test ship and grid having the same size
		// when(captain.getGridTrackingFleet()).thenReturn(grid);
		// assertTrue(grid.addShip(ship4, 'A', 1, ShipOrientation.VERTICAL));
		// //TODO check if not removing ship here
		// grid.removeShip(ship4);
		// assertFalse(grid.addShip(ship4, 'A', 2, ShipOrientation.VERTICAL));
		// assertFalse(grid.addShip(ship4, 'B', 1, ShipOrientation.VERTICAL));
		// assertFalse(grid.addShip(ship4, 'A', 1, ShipOrientation.HORIZONTAL));
		// assertFalse(grid.addShip(ship5, 'A', 1, ShipOrientation.VERTICAL));
		// assertTrue(grid.addShip(ship5, 'A', 1, ShipOrientation.HORIZONTAL));
		// grid.removeShip(ship5);
		//
		// Test normal case
		when(captain.getGridTrackingFleet()).thenReturn(grid);
		assertTrue(grid.addShip(ship6, 'A', 1, ShipOrientation.VERTICAL));
		assertTrue(grid.addShip(ship6, 'B', 2, ShipOrientation.HORIZONTAL));
		// assertFalse(grid.addShip(ship6, 'D', 3, ShipOrientation.HORIZONTAL));
		// assertFalse(grid.addShip(ship6, 'C', 3, ShipOrientation.VERTICAL));
	}
}
