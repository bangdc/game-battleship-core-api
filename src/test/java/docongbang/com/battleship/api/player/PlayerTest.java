package docongbang.com.battleship.api.player;

import org.junit.Test;

import docongbang.com.battleship.api.game.Game;
import docongbang.com.battleship.api.grid.cell.Cell;
import docongbang.com.battleship.api.ship.BasicShip;
import docongbang.com.battleship.api.ship.Ship;
import docongbang.com.battleship.api.utils.ShipOrientation;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class PlayerTest {
	/**
	 * Make sure assertions are enabled with VM argument: -ea
	 */
	@Test(expected = AssertionError.class)
	public void testAssertionsEnabled() {
		assert false;
	}

	@Test
	public void testPlayer() {
		Game game = mock(Game.class);
		Player player = new Player("Do Cong Bang", 3, 7, game);
		assertEquals("Do Cong Bang", player.getName());
		assertEquals(game, player.getGame());
		// by default each player has 5 ships automatically initialized
		assertEquals(5, player.getShips().size());
	}

	@Test
	public void testPlaceShipAt() {
		Game game = mock(Game.class);
		Player player = new Player("Do Cong Bang", 10, 10, game);
		// assertTrue(player.placeShipAt(player.getShips().get(0), 'A', 1,
		// ShipOrientation.HORIZONTAL));
		assertTrue(player.getShips().get(0).wasPlacedOnGrid());

		// assertFalse(player.placeShipAt(player.getShips().get(1), 'A', 1,
		// ShipOrientation.HORIZONTAL));
		assertFalse(player.getShips().get(1).wasPlacedOnGrid());
	}

	@Test(expected = AssertionError.class)
	public void testPlaceShipAt2() {
		Game game = mock(Game.class);
		Player player = new Player("Do Cong Bang", 3, 7, game);
		Ship ship = mock(BasicShip.class);
		// player.placeShipAt(ship, 'A', 0, ShipOrientation.HORIZONTAL);
	}

	@Test
	public void testPlaceShipAuto() {
		Game game = mock(Game.class);
		Player player = new Player("Do Cong Bang", 10, 10, game);
		player.placeShipsAuto();
		for (Ship ship : player.getShips()) {
			assertTrue(ship.wasPlacedOnGrid());
		}
		assertTrue(player.allShipsPlacedOnGrid());
	}

	@Test
	public void testNumberOfTurnsRemaining() {
		Game game = mock(Game.class);
		Player player = new Player("Do Cong Bang", 10, 10, game);

		assertEquals(0, player.getNumberOfTurnsRemaining());
		player.setNumberOfTurnsRemaining(10);
		assertEquals(10, player.getNumberOfTurnsRemaining());
	}

	@Test
	public void testNumberOfConsecutiveFailures() {
		// Game game = mock(Game.class);
		// Player player = new Player("Do Cong Bang", 10, 10, game);
		// assertEquals(0, player.getNumberOfConsecutiveFailures());
		// player.increaseNumberOfConsecutiveFailures();
		// assertEquals(1, player.getNumberOfConsecutiveFailures());
		// player.resetNumberOfConsecutiveFailures();
		// assertEquals(0, player.getNumberOfConsecutiveFailures());
	}

	// @Test(expected=AssertionError.class)
	// public void testReady() {
	// Game game = mock(Game.class);
	// Player player = new Player("Do Cong Bang", 10, 10, game);
	// player.getReady();
	// }
	//
	// @Test(expected=AssertionError.class)
	// public void testReady1() {
	// Game game = mock(Game.class);
	// Player player = new Player("Do Cong Bang", 10, 10, game);
	// Player player2 = new Player("Some one else", 10, 10, game);
	// player.getReady();
	// }
	//
	// @Test(expected=AssertionError.class)
	// public void testReady2() {
	// Game game = mock(Game.class);
	// Player player = new Player("Do Cong Bang", 10, 10, game);
	// player.placeShipsAuto();
	// player.getReady();
	// }
	//
	// @Test
	// public void testReady3() {
	// Game game = mock(Game.class);
	// Player player = new Player("Do Cong Bang", 10, 10, game);
	// Player player2 = new Player("Some one else", 10, 10, game);
	// player.placeShipsAuto();
	// player.getReady();
	// assertTrue(player.isReady());
	// }
}
